/**
 * 
 */

function rowClickedAutor(id) {
	location.href = "/autorDetalji?id=" + id;
}

function dodajKnjiguAutoru(id) {
	location.href = "/addKnjigaAutor?id=" + id;
}

function obrisiKnjiguAutora(id) {
	location.href = "/removeKnjigaAutor?id=" + id;
}

function editAutor(id) {
	location.href = "/editAutor?id=" + id;
}
