/**
 * 
 */

function rowClickedKnjiga(id) {
	location.href = "/knjigaDetalji?id=" + id;
}

function dodajAutoraKnjizi(id) {
	location.href = "/addAutorKnjiga?id=" + id;
}

function obrisiAutoraKnjige(id) {
	location.href = "/removeAutorKnjiga?id=" + id;
}

function editKnjiga(id) {
	location.href = "/editKnjiga?id=" + id;
}

function rowClickedRezervacija(id) {
	location.href = "/posudbaFromRezervacija?id=" + id;
}

function rowClickedPosudba(id) {
	location.href = "/posudbaForm?id=" + id;
}

function korisnikRezervacija(id) {
	location.href = "/korisnikRezervacijaDetalji?id=" + id;
}