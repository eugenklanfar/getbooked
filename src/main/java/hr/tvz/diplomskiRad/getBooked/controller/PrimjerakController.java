package hr.tvz.diplomskiRad.getBooked.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import hr.tvz.diplomskiRad.getBooked.dao.KnjigaRepository;
import hr.tvz.diplomskiRad.getBooked.dao.PrimjerakRepository;
import hr.tvz.diplomskiRad.getBooked.model.Knjiga;
import hr.tvz.diplomskiRad.getBooked.model.Primjerak;

@Controller
public class PrimjerakController {
	
	@Autowired
	private KnjigaRepository knjigaRepository;
	
	@Autowired
	private PrimjerakRepository primjerakRepository;
	
	@RequestMapping(value= "/addPrimjerak", method = RequestMethod.GET)
	String showAddPrimjerak(Model model) {
		
		Primjerak primjerak = new Primjerak();
		List<Knjiga> knjige = knjigaRepository.findByOrderByNazivAsc();
		
		model.addAttribute("primjerak", primjerak);
		model.addAttribute("knjige", knjige);
		return "primjerak/addPrimjerak";
	}
	
	@RequestMapping(value = "/addPrimjerak", method = RequestMethod.POST)
	String newPrimjerak(@Valid Primjerak primjerak, BindingResult bindingResult, @RequestParam Long noviPrimjerak, Model model) {
		
		if (bindingResult.hasErrors()) {
			model.addAttribute("primjerak", primjerak);
			return "primjerak/addPrimjerak";
		}
		Knjiga knjiga = knjigaRepository.findById(noviPrimjerak);
		primjerak.setKnjiga(knjiga);
		primjerakRepository.save(primjerak);
		
		String success = "Primjerak uspješno unesen.";
		model.addAttribute("success",success);
		return "primjerak/addPrimjerak";
	}
	
	@RequestMapping(value = "/addPrimjerak", params = "odustani", method = RequestMethod.POST)
	public String cancelAddPrimjerak() {
		return "redirect:/profile";
	}
	
	@RequestMapping(value= "/addPrimjerakToKnjiga", method = RequestMethod.GET)
	String showAddPrimjerakToKnjiga(@RequestParam ("id") Long id, Model model) {
		
		Primjerak primjerak = new Primjerak();
		Knjiga knjiga = knjigaRepository.findById(id);
		
		primjerak.setKnjiga(knjiga);
		
		model.addAttribute("primjerak", primjerak);
		model.addAttribute("knjigaId", knjiga.getId());
		return "primjerak/addPrimjerakToKnjiga";
	}
	
	@RequestMapping(value = "/addPrimjerakToKnjiga", params = "odustani", method = RequestMethod.POST)
	public String cancelAddPrimjerakToKnjiga(@RequestParam Long knjigaId) {
		return "redirect:/knjigaDetalji?id=" + knjigaId;
	}
	
	@RequestMapping(value = "/addPrimjerakToKnjiga", method = RequestMethod.POST)
	String newPrimjerakToKnjiga(@Valid Primjerak primjerak, BindingResult bindingResult, @RequestParam Long knjigaId, Model model) {
		
		if (bindingResult.hasErrors()) {
			model.addAttribute("primjerak", primjerak);
			model.addAttribute("knjigaId", knjigaId);
			return "redirect:/primjerak/addPrimjerakToKnjiga?id=" + knjigaId;
		}
		
		Knjiga knjiga = knjigaRepository.findById(knjigaId);
		primjerak.setKnjiga(knjiga);
		primjerakRepository.save(primjerak);
		
		String success = "Primjerak uspješno unesen.";
		model.addAttribute("success",success);

		return "redirect:/knjigaDetalji?id=" + knjigaId;
	}

}
