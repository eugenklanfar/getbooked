package hr.tvz.diplomskiRad.getBooked.controller;

import java.util.Date;
import java.util.Set;

import javax.validation.Valid;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import hr.tvz.diplomskiRad.getBooked.config.SecurityUtil;
import hr.tvz.diplomskiRad.getBooked.model.Aktivnost;
import hr.tvz.diplomskiRad.getBooked.model.AktivnostVrsta;
import hr.tvz.diplomskiRad.getBooked.model.Knjiga;
import hr.tvz.diplomskiRad.getBooked.model.Primjerak;
import hr.tvz.diplomskiRad.getBooked.service.AktivnostService;
import hr.tvz.diplomskiRad.getBooked.service.KnjigaService;
import hr.tvz.diplomskiRad.getBooked.service.PrimjerakService;

@Controller
public class RezervacijaController {

	@Value("${app.trajanje.rezervacija}")
	private int trajanjeRezervacije;

	@Autowired
	private KnjigaService knjigaService;

	@Autowired
	private PrimjerakService primjerakService;

	@Autowired
	private AktivnostService aktivnostService;

	@RequestMapping(value = "/rezervirajPrimjerak", method = RequestMethod.GET)
	String showRezervirajPrimjerak(@RequestParam("id") Long id, Model model) {

		final Aktivnost rezervacija = new Aktivnost();

		rezervacija.setDatumOd(new Date());
		rezervacija.setDatumIsteka(DateUtils.addMonths(rezervacija.getDatumOd(), trajanjeRezervacije));
		final Knjiga knjiga = knjigaService.findById(id);
		Set<Primjerak> knjigaPrimjerci = knjiga.getPrimjerci();
		Primjerak primjerak = new Primjerak();
		for (Primjerak primjerakFor : knjigaPrimjerci) {
			if (primjerakFor.isRezervacijaMoguca())
				primjerak = primjerakFor;
		}

		rezervacija.setPrimjerak(primjerak);

		model.addAttribute("rezervacija", rezervacija);
		model.addAttribute("primjerakId", rezervacija.getPrimjerak().getId());

		return "primjerak/rezervirajPrimjerak";
	}

	@RequestMapping(value = "/rezervirajPrimjerak", params = "odustani", method = RequestMethod.POST)
	public String cancelRezervirajPrimjerak(@RequestParam final Long primjerakId) {
		Primjerak primjerak = primjerakService.findById(primjerakId);
		
		return "redirect:/knjigaDetalji?id=" + primjerak.getKnjiga().getId();
	}

	@RequestMapping(value = "/rezervirajPrimjerak", method = RequestMethod.POST)
	public String knjigaRezervacija(@Valid @ModelAttribute("rezervacija") final Aktivnost rezervacija,
			final BindingResult bindingResult, @RequestParam final Long primjerakId, final Model model,
			RedirectAttributes redirAttr) {

		Primjerak primjerak = primjerakService.findById(primjerakId);
		rezervacija.setVrsta(AktivnostVrsta.REZERVACIJA.getAktivnostTip());
		rezervacija.setPrimjerak(primjerak);
		rezervacija.setKorisnickoIme(SecurityUtil.getPrincipal());

		aktivnostService.saveRezervacija(rezervacija);
		redirAttr.addFlashAttribute("success", "Knjiga uspješno rezervirana.");
		return "redirect:/knjigaDetalji?id=" + rezervacija.getPrimjerak().getKnjiga().getId();
	}
}
