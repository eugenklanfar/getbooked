package hr.tvz.diplomskiRad.getBooked.controller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@EnableAutoConfiguration
public class LoginController {
	
	@RequestMapping("/login")
    String login() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			/**
			 * Korisnik je ulogiran 
			 */
		    return "index";
		}
		else 
			return "login";
    }

}
