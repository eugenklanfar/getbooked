package hr.tvz.diplomskiRad.getBooked.controller;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import hr.tvz.diplomskiRad.getBooked.model.ol.OpenLibraryBook;
import hr.tvz.diplomskiRad.getBooked.model.ol.OpenLibraryWrapper;

public class OpenLibraryDeserializer extends JsonDeserializer<OpenLibraryWrapper> {

	@Override
	public OpenLibraryWrapper deserialize(final JsonParser jp, final DeserializationContext ctxt)
	        throws IOException, JsonProcessingException {
		while (jp.nextToken() != JsonToken.END_OBJECT) {
			jp.nextToken();
			if (jp.getCurrentName().contains("ISBN")) {
				final OpenLibraryBook book = ctxt.readValue(jp, OpenLibraryBook.class);
				return new OpenLibraryWrapper(book);
			}
		}
		return null;
	}

}
