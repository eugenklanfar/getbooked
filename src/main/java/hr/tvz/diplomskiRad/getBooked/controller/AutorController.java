package hr.tvz.diplomskiRad.getBooked.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import hr.tvz.diplomskiRad.getBooked.dao.AutorRepository;
import hr.tvz.diplomskiRad.getBooked.dao.KnjigaRepository;
import hr.tvz.diplomskiRad.getBooked.model.Autor;
import hr.tvz.diplomskiRad.getBooked.model.Knjiga;

@Controller
@EnableAutoConfiguration
public class AutorController {
	
	@Autowired
	private AutorRepository autorRepository;
	
	@Autowired
	private KnjigaRepository knjigaRepository;
	
	@RequestMapping(value= "/addAutor", method = RequestMethod.GET)
	String showAddAutor(Model model) {
		Autor autor = new Autor();
		model.addAttribute("autor", autor);
		return "autor/addAutor";
	}
	
	@RequestMapping(value = "/addAutor", method = RequestMethod.POST)
	String newAutor(@Valid Autor autor, BindingResult bindingResult, Model model) {
		
		if (bindingResult.hasErrors()) {
			model.addAttribute("autor", autor);
			return "autor/addAutor";
		}
		String success = "Autor uspješno unesen.";
		autorRepository.save(autor);
		model.addAttribute("success",success);
		return "autor/addAutor";
	}
	
	@RequestMapping(value = "/listaAutora", method = RequestMethod.GET)
	String showListaAutora(Model model) {
		List<Autor> listaAutora = autorRepository.findAll();
		/*for(Autor autor : listaAutora) {
			autor.setDatumRodenja(dateToDateFormat(autor.getDatumRodenja()));
			autor.setDatumSmrti(dateToDateFormat(autor.getDatumSmrti()));
		}*/
		model.addAttribute("autori", listaAutora);
		return "autor/listaAutora";
	}
	
	@RequestMapping(value = "/autorDetalji", method = RequestMethod.GET)
	String showAutorDetalji(@RequestParam ("id") Long id, Model model) {
		Autor autor = new Autor();
		autor = autorRepository.findById(id);
		/*String datumRodenja = "";
		String datumSmrti = "";
		
		datumRodenja = dateFormat(autor.getDatumRodenja());
		datumSmrti = dateFormat(autor.getDatumSmrti());*/
		
		model.addAttribute("autor", autor);
		model.addAttribute("autorId", autor.getId());
		model.addAttribute("imePrezime", autor.getImePrezime());
		model.addAttribute("datumRodenja", autor.getDatumRodenja());
		model.addAttribute("datumSmrti", autor.getDatumSmrti());
		model.addAttribute("openLibraryKey", autor.getOpenLibraryKey());
		model.addAttribute("napomena", autor.getNapomena());
		model.addAttribute("knjige", autor.getKnjige());
		
		return "autor/autorDetalji";
	}
	
	@RequestMapping(value = "/addKnjigaAutor", method = RequestMethod.GET)
	public String showKnjigaAutor(@RequestParam ("id") Long id,Model model) {
		
		Autor autor = new Autor();
		autor = autorRepository.findById(id);
		
		List<Knjiga> knjige = knjigaRepository.findAll();
		/*Set<Knjiga> knjigeAut = autor.getKnjige();
		
		for(Knjiga knjiga : knjige) {
			for(Knjiga knjigaTwo : knjigeAut) {
				if(knjiga == knjigaTwo)
					knjige.remove(knjiga);
			}
		}*/

		model.addAttribute("autorId",id);
		model.addAttribute("imePrezime",autor.getImePrezime());
		model.addAttribute("knjige", knjige);

		return "autor/addKnjigaAutor";
	}
	
	@RequestMapping(value = "/addKnjigaAutor", method = RequestMethod.POST)
	 private String addKnjigaToAutor(@ModelAttribute("autor")Autor autor, 
			BindingResult result, @RequestParam Long autorId, @RequestParam Long novaKnjiga, Model model, RedirectAttributes redirAttr) {
	
		Autor autorPom = autorRepository.findById(autorId);
		Knjiga knjigaPom = knjigaRepository.findOne(novaKnjiga);
		
		knjigaPom.getAutori().add(autorPom);
		knjigaRepository.save(knjigaPom);
	
		String success = "Knjiga uspješno dodana autoru.";
		//model.addAttribute("success", success);
		
		redirAttr.addFlashAttribute("success", success);
		return "redirect:/autorDetalji?id=" + autorPom.getId();
	}
	
	@RequestMapping(value = "/removeKnjigaAutor", method = RequestMethod.GET)
	public String showRemoveAutorKnjiga(@RequestParam ("id") Long id,Model model) {
		
		Autor autor = new Autor();
		autor = autorRepository.findById(id);

		model.addAttribute("autorId",id);
		model.addAttribute("imePrezime",autor.getImePrezime());
		model.addAttribute("knjige", autor.getKnjige());
		
		return "autor/removeKnjigaAutor";
	}
	
	@RequestMapping(value = "/removeKnjigaAutor", method = RequestMethod.POST)
	 private String removeKnjigaFromAutor(@ModelAttribute("autor")Autor autor, 
			BindingResult result, @RequestParam Long autorId, @RequestParam Long novaKnjiga, Model model, RedirectAttributes redirAttr) {
	
		Autor autorPom = autorRepository.findById(autorId);
		Knjiga knjigaPom = knjigaRepository.findOne(novaKnjiga);
		
		knjigaPom.getAutori().remove(autorPom);
		knjigaRepository.save(knjigaPom);
	
		String success = "Knjiga uspješno uklonjena.";
		//model.addAttribute("success", success);
		
		redirAttr.addFlashAttribute("success", success);
		return "redirect:/autorDetalji?id=" + autorPom.getId();
	}
	
	@RequestMapping(value = "/editAutor", method = RequestMethod.GET)
	String showEditAutor(@RequestParam ("id") Long id, Model model) {
		Autor autor = new Autor();
		autor = autorRepository.findById(id);
		
		/*String datumRodenja = "";
		String datumSmrti = "";
		
		datumRodenja = dateFormat(autor.getDatumRodenja());
		datumSmrti = dateFormat(autor.getDatumSmrti());*/
		
		model.addAttribute("autor", autor);
		model.addAttribute("autorId", autor.getId());
		model.addAttribute("imePrezime", autor.getImePrezime());
		model.addAttribute("datumRodenja", autor.getDatumRodenja());
		model.addAttribute("datumSmrti", autor.getDatumSmrti());
		model.addAttribute("openLibraryKey", autor.getOpenLibraryKey());
		model.addAttribute("napomena", autor.getNapomena());
		return "autor/editAutor";
	}
	
	@RequestMapping(value = "/editAutor", method = RequestMethod.POST)
	 private String editAutor(@ModelAttribute("autor")Autor autor, 
			BindingResult result, @RequestParam Long autorId, @RequestParam String imePrezime, @RequestParam String openLibraryKey,
			@RequestParam String datumRodenja, @RequestParam String datumSmrti, @RequestParam String napomena, Model model, RedirectAttributes redirAttr) {
	
		Autor autorPom = autorRepository.findById(autorId);
		
		autorPom.setImePrezime(imePrezime);
		autorPom.setDatumRodenja(datumRodenja);
		autorPom.setDatumSmrti(datumSmrti);
		autorPom.setNapomena(napomena);
		autorPom.setOpenLibraryKey(openLibraryKey);
		
		autorRepository.save(autorPom);
	
		String success = "Autor uspješno ažuriran.";
		//model.addAttribute("success", success);
	
		redirAttr.addFlashAttribute("success", success);
		return "redirect:/autorDetalji?id=" + autorPom.getId();
	}
	
	@RequestMapping(value = "/editAutor", params = "odustani", method = RequestMethod.POST)
	public String cancelEditAutor(@RequestParam Long autorId) {
		return "redirect:/autorDetalji?id=" + autorId;
	}
	
	@RequestMapping(value = "/addKnjigaAutor", params = "odustani", method = RequestMethod.POST)
	public String cancelAddKnjigaAutor(@RequestParam Long autorId) {
		return "redirect:/autorDetalji?id=" + autorId;
	}
	
	@RequestMapping(value = "/removeKnjigaAutor", params = "odustani", method = RequestMethod.POST)
	public String cancelRemoveKnjigaAutor(@RequestParam Long autorId) {
		return "redirect:/autorDetalji?id=" + autorId;
	}

}
