package hr.tvz.diplomskiRad.getBooked.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import hr.tvz.diplomskiRad.getBooked.dao.KomentarRepository;
import hr.tvz.diplomskiRad.getBooked.dao.KorisnikRepository;
import hr.tvz.diplomskiRad.getBooked.dao.VijestRepository;
import hr.tvz.diplomskiRad.getBooked.model.Komentar;
import hr.tvz.diplomskiRad.getBooked.model.Korisnik;
import hr.tvz.diplomskiRad.getBooked.model.Vijest;
import hr.tvz.diplomskiRad.getBooked.service.CustomUserDetails;

@Controller
public class VijestController {

	@Autowired
	private VijestRepository vijestRepository;

	@Autowired
	private KorisnikRepository korisnikRepository;
	
	@Autowired
	private KomentarRepository komentarRepository;


	@RequestMapping(value = "/vijestForm", method = RequestMethod.GET)
	String showVijestForm(Model model) {
		Vijest vijest = new Vijest();
		model.addAttribute("vijest", vijest);
		return "vijest/vijestForm";
	}

	@RequestMapping(value = "/vijestForm", params = "odustani", method = RequestMethod.POST)
	public String cancelEditAutor() {
		return "redirect:/index";
	}

	@RequestMapping(value = "/vijestForm", method = RequestMethod.POST)
	String newAutor(@ModelAttribute("vijest") Vijest vijest, BindingResult bindingResult, Model model) {

		if (bindingResult.hasErrors()) {
			String error = "Dogodila se pogreška.";
			model.addAttribute("error", error);
			model.addAttribute("vijest", vijest);
			return "vijest/vijestForm";
		}

		CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Korisnik korisnikPom = korisnikRepository.findById(user.getUserId());

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();

		vijest.setImePrezime(korisnikPom.getIme() + " " + korisnikPom.getPrezime());
		vijest.setDatumObjave(dateFormat.format(date));

		vijestRepository.save(vijest);

		String success = "Novost uspješno unesena.";
		
		int brojac = 0;
		int tekstSize = 0;
		String tekst = "";
		List<Vijest> vijestiPom = vijestRepository.findByOrderByDatumObjaveDesc();
		List<Vijest> vijesti = new ArrayList<Vijest>();

		for (int i = 0; i < vijestiPom.size(); i++) {
			tekstSize = vijestiPom.get(i).getTekst().length();
			tekst = vijestiPom.get(i).getTekst().substring(0, tekstSize / 2);
			vijestiPom.get(i).setTekst(tekst);
		}

		if (vijestiPom.size() < 10)
			model.addAttribute("vijesti", vijestiPom);

		else {
			while (brojac < 10) {
				vijesti.add(vijestiPom.get(brojac));
				brojac++;
			}
			model.addAttribute("vijesti", vijesti);
		}

		model.addAttribute("korisnik", korisnikPom);
		model.addAttribute("success", success);
		return "/index";
	}

	@RequestMapping(value={"/", "/index", ""}, method = RequestMethod.GET)
	String showIndex(Model model) {

		CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Korisnik korisnik = korisnikRepository.findById(user.getUserId());

		int brojac = 0;
		int tekstSize = 0;
		String tekst = "";
		List<Vijest> vijestiPom = vijestRepository.findByOrderByDatumObjaveDesc();
		List<Vijest> vijesti = new ArrayList<Vijest>();

		for (int i = 0; i < vijestiPom.size(); i++) {
			tekstSize = vijestiPom.get(i).getTekst().length();
			tekst = vijestiPom.get(i).getTekst().substring(0, tekstSize / 2);
			vijestiPom.get(i).setTekst(tekst);
		}

		if (vijestiPom.size() < 10)
			model.addAttribute("vijesti", vijestiPom);

		else {
			while (brojac < 10) {
				vijesti.add(vijestiPom.get(brojac));
				brojac++;
			}
			model.addAttribute("vijesti", vijesti);
		}

		model.addAttribute("korisnik", korisnik);

		return "index";
	}

	@RequestMapping(value = "/vijestDetalji", method = RequestMethod.GET)
	String showVijestDetalji(@RequestParam Long id, Model model) {
		Vijest vijest = vijestRepository.findOne(id);
		Komentar komentar = new Komentar();

		model.addAttribute("vijestId", id);
		model.addAttribute("komentar", komentar);
		model.addAttribute("vijest", vijest);
		model.addAttribute("komentari", vijest.getKomentari());
		return "vijest/vijestDetalji";
	}

	@RequestMapping(value = "/addKomentarToVijest", method = RequestMethod.POST)
	private String addKomentarToVijest(@ModelAttribute("komentar") Komentar komentar, BindingResult bindingResult,
			@RequestParam Long vijestId, Model model, RedirectAttributes redirAttr) {

		if (bindingResult.hasErrors()) {
			String error = "Dogodila se pogreška.";
			redirAttr.addFlashAttribute("error", error);
			return "redirect:/vijestDetalji?id=" + vijestId;
		}

		CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Korisnik korisnikPom = korisnikRepository.findById(user.getUserId());
		Vijest vijestPom = vijestRepository.findOne(vijestId);

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();

		komentar.setKorisnik(korisnikPom);
		komentar.setVijest(vijestPom);
		komentar.setDatumObjave(dateFormat.format(date));
		komentarRepository.save(komentar);
		String success = "Komentar uspješno unesen.";
		redirAttr.addFlashAttribute("success", success);

		return "redirect:/vijestDetalji?id=" + vijestPom.getId();
	}
}
