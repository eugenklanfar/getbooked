package hr.tvz.diplomskiRad.getBooked.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import hr.tvz.diplomskiRad.getBooked.model.Korisnik;
import hr.tvz.diplomskiRad.getBooked.service.KorisnikService;

@Controller
public class RegisterController {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private KorisnikService korisnikService;
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	String showRegistration(Model model) {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (!(authentication instanceof AnonymousAuthenticationToken)) {

			/**
			 * Korisnik je ulogiran
			 */
			return "index";
		} else {
			Korisnik korisnik = new Korisnik();
			model.addAttribute("korisnik", korisnik);
			return "register";
		}
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	String userRegister(@Valid Korisnik korisnik, BindingResult bindingResult, @RequestParam String ponovljenaLozinka,
			Model model) {
		Boolean korisnikPostoji = false;
		Iterable<Korisnik> korisnici = korisnikService.findAll();

		for (Korisnik k : korisnici) {
			if (korisnik.getKorisnickoIme().equals(k.getKorisnickoIme()))
				korisnikPostoji = true;
		}

		if (bindingResult.getErrorCount() > 0 || korisnikPostoji || !korisnik.getLozinka().equals(ponovljenaLozinka)) {
			model.addAttribute("korisnik", korisnik);

			if (!korisnik.getLozinka().equals(ponovljenaLozinka)) {
				String nepodudaranje = "Unesene lozinke se ne podudaraju.";
				model.addAttribute("nepodudaranje", nepodudaranje);
			}

			if (korisnikPostoji) {
				String postoji = "Uneseno korisnicko ime vec postoji.";
				model.addAttribute("postoji", postoji);
			}

			return "register";
		}

		korisnik.setLozinka(passwordEncoder.encode(korisnik.getLozinka()));
		korisnikService.save(korisnik);
		String registrationSuccess = "Registracija uspješna.";
		model.addAttribute("registrationSuccess", registrationSuccess);
		return "login";
	}

}
