package hr.tvz.diplomskiRad.getBooked.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import hr.tvz.diplomskiRad.getBooked.dao.AktivnostRepository;
import hr.tvz.diplomskiRad.getBooked.model.Aktivnost;
import hr.tvz.diplomskiRad.getBooked.model.AktivnostVrsta;
import hr.tvz.diplomskiRad.getBooked.service.AktivnostService;
import hr.tvz.diplomskiRad.getBooked.service.KorisnikService;

@Controller
public class AktivnostController {

	@Value("${app.trajanje.posudba}")
	private int trajanjePosudbe;

	@Autowired
	private AktivnostRepository aktivnostRepository;

	@Autowired
	private AktivnostService aktivnostService;

	@Autowired
	private KorisnikService korisnikService;

	@RequestMapping(value = "/listaAktivnosti", method = RequestMethod.GET)
	String showListaAktivnosti(Model model) {

		return dohvatiAktivnosti(model);
	}

	@RequestMapping(value = "/posudbaFromRezervacija", method = RequestMethod.GET)
	public String showFormPosudbaRezervacija(@RequestParam final Long id, final Model model) {

		final Aktivnost rezervacija = aktivnostService.getAktivnostById(id);

		final Aktivnost posudba = new Aktivnost();
		posudba.setDatumOd(new Date());
		posudba.setDatumIsteka(DateUtils.addMonths(posudba.getDatumOd(), trajanjePosudbe));
		posudba.setPrimjerak(rezervacija.getPrimjerak());
		posudba.setKorisnickoIme(rezervacija.getKorisnickoIme());
		posudba.setKorisnik(korisnikService.findByKorisnickoIme(posudba.getKorisnickoIme()));

		model.addAttribute("aktivnostId", rezervacija.getId());
		model.addAttribute("aktivnost", posudba);
		model.addAttribute("vrstaAktivnosti", "rezervacija");
		return "aktivnost/posudbaForm";
	}

	@RequestMapping(value = "/posudbaForm", params = "odustani", method = RequestMethod.POST)
	public String cancelPosudbaForm(Model model) {
		return dohvatiAktivnosti(model);
	}

	@RequestMapping(value = "/posudbaForm", params = "posudi", method = RequestMethod.POST)
	public String knjigaPosudbaFromRezervacija(@Valid @ModelAttribute("aktivnost") final Aktivnost aktivnost,
			final BindingResult bindingResult, @RequestParam final Long aktivnostId, final Model model) {

		aktivnostService.savePosudbaFromRezervacija(aktivnost, aktivnostId);

		model.addAttribute("success", "Knjiga uspješno posuđena.");

		return dohvatiAktivnosti(model);
	}

	@RequestMapping(value = "/posudbaForm", params = "zatvori", method = RequestMethod.POST)
	public String cancelRezervacija(@Valid @ModelAttribute("aktivnost") final Aktivnost aktivnost,
			final BindingResult bindingResult, @RequestParam final Long aktivnostId, final Model model) {

		final Aktivnost aktivnostIzBaze = aktivnostService.getAktivnostById(aktivnostId);
		/**
		 * Za korisnikovo zatvaranje rezervacije
		 */
		/*
		 * if (!aktivnostIzBaze.getKorisnickoIme().equals(SecurityUtil.getPrincipal()))
		 * { model.addAttribute("greska",
		 * "Ne možete zatvoriti rezervaciju koja nije rezervirana na Vas."); return
		 * "aktivnost/posudbaForm"; }
		 */

		model.addAttribute("success", "Rezervacija uspješno zatvorena.");

		aktivnostService.zatvaranjeRezervacije(aktivnostIzBaze);
		return dohvatiAktivnosti(model);
	}

	/**
	 * Posudba
	 * 
	 * @param id
	 * @param model
	 * @return
	 */

	@RequestMapping(value = "/posudbaForm", method = RequestMethod.GET)
	public String showPosudbaForm(@RequestParam final Long id, final Model model) {

		final Aktivnost posudba = aktivnostService.getAktivnostById(id);
		posudba.setDatumDo(new Date());
		posudba.setKorisnik(korisnikService.findByKorisnickoIme(posudba.getKorisnickoIme()));

		model.addAttribute("aktivnostId", posudba.getId());
		model.addAttribute("aktivnost", posudba);
		model.addAttribute("vrstaAktivnosti", "posudba");
		return "aktivnost/posudbaForm";
	}

	@RequestMapping(value = "/posudbaForm", params = "zatvoriPosudbu", method = RequestMethod.POST)
	public String cancelPosudba(@Valid @ModelAttribute("aktivnost") final Aktivnost aktivnost,
			final BindingResult bindingResult, @RequestParam final Long aktivnostId, final Model model) {

		aktivnostService.zatvaranjePosudbe(aktivnostId, aktivnost.getDatumDo());

		model.addAttribute("success", "Posudba uspješno zatvorena.");

		return dohvatiAktivnosti(model);
	}

	public String dohvatiAktivnosti(Model model) {

		List<Aktivnost> posudeneAktivnosti = aktivnostRepository
				.findByVrstaAndDatumDoIsNull(AktivnostVrsta.POSUDBA.getAktivnostTip());
		List<Aktivnost> rezerviraneAktivnosti = aktivnostRepository
				.findByVrstaAndDatumDoIsNull(AktivnostVrsta.REZERVACIJA.getAktivnostTip());

		model.addAttribute("posudeneAktivnosti", posudeneAktivnosti);
		model.addAttribute("rezerviraneAktivnosti", rezerviraneAktivnosti);

		return "aktivnost/listaAktivnosti";
	}

}
