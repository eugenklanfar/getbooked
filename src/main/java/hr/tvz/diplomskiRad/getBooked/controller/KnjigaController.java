package hr.tvz.diplomskiRad.getBooked.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import hr.tvz.diplomskiRad.getBooked.dao.AutorRepository;
import hr.tvz.diplomskiRad.getBooked.dao.KnjigaRepository;
import hr.tvz.diplomskiRad.getBooked.dao.KomentarRepository;
import hr.tvz.diplomskiRad.getBooked.dao.KorisnikRepository;
import hr.tvz.diplomskiRad.getBooked.dao.OcjenaRepository;
import hr.tvz.diplomskiRad.getBooked.dao.PovijestCitanjaRepository;
import hr.tvz.diplomskiRad.getBooked.model.Autor;
import hr.tvz.diplomskiRad.getBooked.model.Knjiga;
import hr.tvz.diplomskiRad.getBooked.model.Komentar;
import hr.tvz.diplomskiRad.getBooked.model.Korisnik;
import hr.tvz.diplomskiRad.getBooked.model.Ocjena;
import hr.tvz.diplomskiRad.getBooked.model.PovijestCitanja;
import hr.tvz.diplomskiRad.getBooked.model.PovijestCitanjaVrsta;
import hr.tvz.diplomskiRad.getBooked.model.Primjerak;
import hr.tvz.diplomskiRad.getBooked.model.Zanr;
import hr.tvz.diplomskiRad.getBooked.service.AutorService;
import hr.tvz.diplomskiRad.getBooked.service.CustomUserDetails;
import hr.tvz.diplomskiRad.getBooked.service.PovijestCitanjaService;
import hr.tvz.diplomskiRad.getBooked.service.ZanrService;

@Controller
@EnableAutoConfiguration
public class KnjigaController {

	@Autowired
	private KnjigaRepository knjigaRepository;

	@Autowired
	private AutorRepository autorRepository;

	@Autowired
	private KorisnikRepository korisnikRepository;

	@Autowired
	private PovijestCitanjaRepository povijestCitanjaRepository;

	@Autowired
	private PovijestCitanjaService povijestCitanjaService;

	@Autowired
	private AutorService autorService;

	@Autowired
	private OcjenaRepository ocjenaRepository;

	@Autowired
	private KomentarRepository komentarRepository;

	@Autowired
	private ZanrService zanrService;

	@RequestMapping(value = "/addKnjiga", method = RequestMethod.GET)
	String showAddKnjiga(Model model) {
		Knjiga knjiga = new Knjiga();

		List<Zanr> zanrovi = zanrService.findAll();

		model.addAttribute("zanrovi", zanrovi);
		model.addAttribute("knjiga", knjiga);
		model.addAttribute("autori", autorRepository.findAll());
		return "knjiga/addknjiga";
	}

	@RequestMapping(value = "/addKnjiga", method = RequestMethod.POST)
	String saveNewKnjiga(@ModelAttribute("knjiga") @Valid final Knjiga knjiga, final BindingResult bindingResult,
			final Model model, @RequestParam(value = "noviZanrovi", required = false) List<Zanr> noviZanrovi,
			@RequestParam(value = "noviAutori", required = true) List<Autor> noviAutori) {

		if (bindingResult.hasErrors()) {
			String error = "Dogodila se pogreška.";
			model.addAttribute("error", error);
			model.addAttribute("knjiga", knjiga);
			return "knjiga/addKnjiga";
		}
		if (knjigaRepository.findByISBN(knjiga.getiSBN()) != null) {
			String error = "Postoji knjiga s unesenim ISBN-om.";
			List<Zanr> zanrovi = zanrService.findAll();

			model.addAttribute("zanrovi", zanrovi);
			model.addAttribute("autori", autorRepository.findAll());
			model.addAttribute("error", error);
			return "knjiga/addKnjiga";
		}

		knjiga.setZanrovi(noviZanrovi);
		knjiga.setAutori(noviAutori);

		knjigaRepository.save(knjiga);
		String success = "Knjiga uspješno unesena.";
		model.addAttribute("success", success);

		return "knjiga/addKnjiga";
	}

	@RequestMapping(value = "/listaKnjiga", method = RequestMethod.GET)
	String showListaKnjiga(Model model) {
		List<Knjiga> listaKnjiga = knjigaRepository.findAll();

		for (Knjiga knjiga : listaKnjiga) {

			float srednjaOcjena = 0;
			int srednjaOcjenaInt = 0;
			int zbroj = 0;
			String prosjecnaOcjena = "Not Rated Yet";

			Set<Ocjena> ocjene = new HashSet<Ocjena>();

			if (knjiga.getOcjene() != null) {
				ocjene = knjiga.getOcjene();

				for (Ocjena ocjenaF : ocjene) {
					zbroj += ocjenaF.getOcjena();
				}
				if (zbroj > 0) {
					srednjaOcjenaInt = zbroj / ocjene.size();
					srednjaOcjena = (float) zbroj / ocjene.size();
					prosjecnaOcjena = srednjaOcjena + " od " + ocjene.size() + " Korisnika";
				}
			}

			knjiga.setProsjecnaOcjena(srednjaOcjena);
		}
		model.addAttribute("knjige", listaKnjiga);
		return "knjiga/listaKnjiga";
	}

	@RequestMapping(value = "/knjigaDetalji", method = RequestMethod.GET)
	String showKnjigaDetalji(@RequestParam("id") Long id, Model model) {
		CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Korisnik korisnik = korisnikRepository.findById(user.getUserId());
		Komentar komentar = new Komentar();

		Knjiga knjiga = new Knjiga();
		Ocjena ocjenaKor = new Ocjena();

		PovijestCitanja povijestCitanjaKor = new PovijestCitanja();
		knjiga = knjigaRepository.findById(id);

		List<Integer> ocjeneForm = new ArrayList<Integer>();
		Set<Ocjena> ocjene = new HashSet<Ocjena>();
		Set<Primjerak> primjerci = new HashSet<Primjerak>();
		int brojPrimjeraka = 0;

		float srednjaOcjena = 0;
		int srednjaOcjenaInt = 0;
		int zbroj = 0;
		String prosjecnaOcjena = "Not Rated Yet";

		for (int i = 1; i <= 5; i++) {
			ocjeneForm.add(i);
		}

		if (ocjenaRepository.findByKorisnikAndKnjiga(korisnik, knjiga) != null) {
			ocjenaKor = ocjenaRepository.findByKorisnikAndKnjiga(korisnik, knjiga);
			model.addAttribute("ocjenaKor", ocjenaKor.getOcjena());
		}

		if (knjiga.getOcjene() != null) {
			ocjene = knjiga.getOcjene();

			for (Ocjena ocjenaF : ocjene) {
				zbroj += ocjenaF.getOcjena();
			}
			if (zbroj > 0) {
				srednjaOcjenaInt = zbroj / ocjene.size();
				srednjaOcjena = (float) zbroj / ocjene.size();
				prosjecnaOcjena = srednjaOcjena + " od " + ocjene.size() + " Korisnika";
			}
		}

		String zanrovi = "";
		int i = 0;
		for (Zanr zanr : knjiga.getZanrovi()) {
			if (i++ == knjiga.getZanrovi().size() - 1)
				zanrovi = zanrovi + zanr.getZanr();
			else
				zanrovi = zanrovi + zanr.getZanr() + ", ";
		}

		List<PovijestCitanjaVrsta> povijestCitanjaList = new ArrayList<PovijestCitanjaVrsta>(
				EnumSet.allOf(PovijestCitanjaVrsta.class));
		model.addAttribute("povijestiCitanja", povijestCitanjaList);

		if (povijestCitanjaService.findByKorisnikAndKnjiga(korisnik, knjiga) != null) {
			povijestCitanjaKor = povijestCitanjaService.findByKorisnikAndKnjiga(korisnik, knjiga);
			model.addAttribute("povijestCitanjaKor", povijestCitanjaKor.getVrsta());
		}

		primjerci = knjiga.getPrimjerci();

		for (Primjerak primjerak : primjerci) {
			if (primjerak.isRezervacijaMoguca())
				brojPrimjeraka++;
		}

		if (brojPrimjeraka > 0)
			model.addAttribute("rezervacijaDostupna", "true");
		else
			model.addAttribute("rezervacijaDostupna", "false");

		model.addAttribute("knjiga", knjiga);
		model.addAttribute("knjigaId", knjiga.getId());
		model.addAttribute("naziv", knjiga.getNaziv());
		model.addAttribute("iSBN", knjiga.getiSBN());
		model.addAttribute("opis", knjiga.getOpis());
		model.addAttribute("materijalniOpis", knjiga.getMaterijalniOpis());
		model.addAttribute("godinaIzdavanja", knjiga.getGodinaIzdavanja());
		model.addAttribute("jezik", knjiga.getJezik());
		model.addAttribute("izdavac", knjiga.getIzdavac());
		model.addAttribute("openLibraryKey", knjiga.getOpenLibraryKey());
		model.addAttribute("autori", knjiga.getAutori());
		model.addAttribute("zanrovi", zanrovi);
		model.addAttribute("komentari", knjiga.getKomentari());
		model.addAttribute("ocjeneForm", ocjeneForm);
		model.addAttribute("prosjecnaOcjena", prosjecnaOcjena);
		model.addAttribute("srednjaOcjena", srednjaOcjena);
		model.addAttribute("komentar", komentar);
		model.addAttribute("primjerci", brojPrimjeraka);

		return "knjiga/knjigaDetalji";
	}

	@RequestMapping(value = "/knjigaDetalji", method = RequestMethod.POST)
	private String addPovijest(@ModelAttribute("knjiga") Knjiga knjiga, BindingResult result,
			@RequestParam Long knjigaId, @RequestParam String novaPovijestCitanja, @RequestParam String akcija,
			Model model, RedirectAttributes redirAttr) {

		Knjiga knjigaP = knjigaRepository.findById(knjigaId);
		if (akcija.equals("Dodaj")) {
			CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			Korisnik korisnikPom = korisnikRepository.findById(user.getUserId());
			Knjiga knjigaPom = knjigaRepository.findById(knjigaId);
			PovijestCitanja povijestCitanja = new PovijestCitanja();

			if (povijestCitanjaRepository.findByKorisnikAndKnjiga(korisnikPom, knjigaPom) != null) {
				povijestCitanja = povijestCitanjaRepository.findByKorisnikAndKnjiga(korisnikPom, knjigaPom);
				if (povijestCitanja.getVrsta().equals(novaPovijestCitanja)) {
					String success = "Aktivnost vec postoji.";
					redirAttr.addFlashAttribute("success", success);
					return "redirect:/knjigaDetalji?id=" + knjigaPom.getId();
				} else {
					povijestCitanja.setVrsta(novaPovijestCitanja);
					povijestCitanjaService.save(povijestCitanja);
					String success = "Aktivnost uspješno promijenjena.";
					model.addAttribute("success", success);
					redirAttr.addFlashAttribute("success", success);
					return "redirect:/knjigaDetalji?id=" + knjigaPom.getId();
				}
			} else {
				povijestCitanja.setKnjiga(knjigaPom);
				povijestCitanja.setKorisnik(korisnikPom);
				povijestCitanja.setVrsta(novaPovijestCitanja);

				povijestCitanjaService.save(povijestCitanja);
				String success = "Aktivnost uspješno spremljena.";
				redirAttr.addFlashAttribute("success", success);
				return "redirect:/knjigaDetalji?id=" + knjigaPom.getId();
			}
		}

		else if (akcija.equals("Promijeni"))
			return "redirect:/editKnjiga?id=" + knjigaId;
		else if (akcija.equals("Rezerviraj"))
			return "redirect:/rezervirajPrimjerak?id=" + knjigaId;
		else if (akcija.equals("Dodaj primjerak"))
			return "redirect:/addPrimjerakToKnjiga?id=" + knjigaId;
		return "redirect:/knjigaDetalji?id=" + knjigaP.getId();
	}

	@RequestMapping(value = "/editKnjiga", method = RequestMethod.GET)
	String showEditKnjiga(@RequestParam("id") Long id, Model model) {
		Knjiga knjiga = new Knjiga();
		knjiga = knjigaRepository.findById(id);

		List<Zanr> zanrovi = zanrService.findAll();
		List<Zanr> knjigaZanrovi = knjiga.getZanrovi();
		List<Autor> autori = autorService.findAll();
		List<Autor> knjigaAutori = knjiga.getAutori();

		for (Zanr zanr : knjigaZanrovi) {
			zanrovi.remove(zanr);
		}

		for (Autor autor : knjigaAutori) {
			autori.remove(autor);
		}

		model.addAttribute("knjigaZanrovi", knjigaZanrovi);
		model.addAttribute("zanrovi", zanrovi);
		model.addAttribute("knjiga", knjiga);
		model.addAttribute("knjigaId", knjiga.getId());
		model.addAttribute("naziv", knjiga.getNaziv());
		model.addAttribute("iSBN", knjiga.getiSBN());
		model.addAttribute("opis", knjiga.getOpis());
		model.addAttribute("materijalniOpis", knjiga.getMaterijalniOpis());
		model.addAttribute("godinaIzdavanja", knjiga.getGodinaIzdavanja());
		model.addAttribute("jezik", knjiga.getJezik());
		model.addAttribute("izdavac", knjiga.getIzdavac());
		model.addAttribute("openLibraryKey", knjiga.getOpenLibraryKey());
		model.addAttribute("knjigaAutori", knjiga.getAutori());
		model.addAttribute("autori", autori);
		return "knjiga/editKnjiga";
	}

	@RequestMapping(value = "/editKnjiga", method = RequestMethod.POST)
	private String editKnjiga(@ModelAttribute("knjiga") Knjiga knjiga, BindingResult result,
			@RequestParam Long knjigaId, @RequestParam String naziv, @RequestParam String iSBN,
			@RequestParam String opis, @RequestParam String materijalniOpis, @RequestParam String godinaIzdavanja,
			@RequestParam String jezik, @RequestParam String izdavac, @RequestParam String openLibraryKey,
			@RequestParam List<Zanr> noviZanrovi, @RequestParam List<Autor> noviAutori, Model model,
			RedirectAttributes redirAttr) {

		Knjiga knjigaPom = knjigaRepository.findById(knjigaId);

		knjigaPom.setGodinaIzdavanja(godinaIzdavanja);
		knjigaPom.setiSBN(iSBN);
		knjigaPom.setIzdavac(izdavac);
		knjigaPom.setJezik(jezik);
		knjigaPom.setMaterijalniOpis(materijalniOpis);
		knjigaPom.setNaziv(naziv);
		knjigaPom.setOpenLibraryKey(openLibraryKey);
		knjigaPom.setOpis(opis);
		knjigaPom.setZanrovi(noviZanrovi);

		if (noviAutori != null)
			knjigaPom.setAutori(noviAutori);

		knjigaRepository.save(knjigaPom);

		String success = "Knjiga uspješno ažurirana.";
		// model.addAttribute("success", success);

		redirAttr.addFlashAttribute("success", success);
		return "redirect:/knjigaDetalji?id=" + knjigaPom.getId();

		// return "index";
	}

	@RequestMapping(value = "/addKomentar", method = RequestMethod.POST)
	private String addKomentar(@ModelAttribute("komentar") Komentar komentar, BindingResult bindingResult,
			@RequestParam Long knjigaId, @RequestParam String akcija,
			@RequestParam(value = "novaOcjena", required = false, defaultValue = "0") int novaOcjena, Model model,
			RedirectAttributes redirAttr) {
		if (bindingResult.hasErrors()) {
			String error = "ERROR";
			redirAttr.addFlashAttribute("error", error);
			return "redirect:listaKnjiga";
		}

		else if (akcija.equals("Ocijeni")) {
			CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			Korisnik korisnikPom = korisnikRepository.findById(user.getUserId());
			Knjiga knjigaPom = knjigaRepository.findById(knjigaId);
			Ocjena ocjena = new Ocjena();
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = new Date();

			if (ocjenaRepository.findByKorisnikAndKnjiga(korisnikPom, knjigaPom) != null) {
				ocjena = ocjenaRepository.findByKorisnikAndKnjiga(korisnikPom, knjigaPom);
				ocjena.setKnjiga(knjigaPom);
				ocjena.setKorisnik(korisnikPom);
				ocjena.setOcjena(novaOcjena);
				ocjena.setDatumOcjene(dateFormat.format(date));
				ocjenaRepository.save(ocjena);

				String success = "Ocjena uspješno promijenjena.";
				redirAttr.addFlashAttribute("success", success);
				return "redirect:/knjigaDetalji?id=" + knjigaPom.getId();
			}

			else {
				ocjena.setKnjiga(knjigaPom);
				ocjena.setKorisnik(korisnikPom);
				ocjena.setOcjena(novaOcjena);
				ocjena.setDatumOcjene(dateFormat.format(date));

				ocjenaRepository.save(ocjena);

				String success = "Knjiga uspješno ocijenjena.";
				redirAttr.addFlashAttribute("success", success);
				return "redirect:/knjigaDetalji?id=" + knjigaPom.getId();
			}

		}

		else if (akcija.equals("Add")) {

			CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			Korisnik korisnikPom = korisnikRepository.findById(user.getUserId());
			Knjiga knjigaPom = knjigaRepository.findById(knjigaId);

			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = new Date();

			komentar.setKorisnik(korisnikPom);
			komentar.setKnjiga(knjigaPom);
			komentar.setDatumObjave(dateFormat.format(date));
			komentarRepository.save(komentar);
			String success = "Komentar uspješno unesen.";
			redirAttr.addFlashAttribute("success", success);
			// return "redirect:listaKnjiga";
			return "redirect:/knjigaDetalji?id=" + knjigaPom.getId();
		}

		return "redirect:/knjigaDetalji?id=" + knjigaId;
	}

	@RequestMapping(value = "/editKnjiga", params = "odustani", method = RequestMethod.POST)
	public String cancelEditKnjiga(@RequestParam Long knjigaId) {
		return "redirect:/knjigaDetalji?id=" + knjigaId;
	}

}
