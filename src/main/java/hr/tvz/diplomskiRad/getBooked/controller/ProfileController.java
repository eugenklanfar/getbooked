package hr.tvz.diplomskiRad.getBooked.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import hr.tvz.diplomskiRad.getBooked.dao.KnjigaRepository;
import hr.tvz.diplomskiRad.getBooked.dao.PovijestCitanjaRepository;
import hr.tvz.diplomskiRad.getBooked.model.Aktivnost;
import hr.tvz.diplomskiRad.getBooked.model.AktivnostVrsta;
import hr.tvz.diplomskiRad.getBooked.model.Drzava;
import hr.tvz.diplomskiRad.getBooked.model.Knjiga;
import hr.tvz.diplomskiRad.getBooked.model.Komentar;
import hr.tvz.diplomskiRad.getBooked.model.Korisnik;
import hr.tvz.diplomskiRad.getBooked.model.Ocjena;
import hr.tvz.diplomskiRad.getBooked.model.PovijestCitanja;
import hr.tvz.diplomskiRad.getBooked.model.PovijestCitanjaVrsta;
import hr.tvz.diplomskiRad.getBooked.model.Zanr;
import hr.tvz.diplomskiRad.getBooked.service.AktivnostService;
import hr.tvz.diplomskiRad.getBooked.service.CustomUserDetails;
import hr.tvz.diplomskiRad.getBooked.service.DrzavaService;
import hr.tvz.diplomskiRad.getBooked.service.KomentarService;
import hr.tvz.diplomskiRad.getBooked.service.KorisnikService;
import hr.tvz.diplomskiRad.getBooked.service.OcjenaService;
import hr.tvz.diplomskiRad.getBooked.service.ZanrService;

@Controller
@EnableAutoConfiguration
public class ProfileController {

	@Autowired
	private AktivnostService aktivnostService;

	@Autowired
	private KorisnikService korisnikService;

	@Autowired
	private PovijestCitanjaRepository povijestCitanjaRepository;

	@Autowired
	private KnjigaRepository knjigaRepository;

	@Autowired
	private DrzavaService drzavaService;

	@Autowired
	private ZanrService zanrService;

	@Autowired
	private KomentarService komentarService;

	@Autowired
	private OcjenaService ocjenaService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	String showUserProfile(Model model) {
		CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Korisnik korisnik = korisnikService.findById(user.getUserId());

		List<Aktivnost> posudeneAktivnosti = aktivnostService.findByKorisnickoImeAndVrstaAndDatumDoIsNull(
				korisnik.getKorisnickoIme(), AktivnostVrsta.POSUDBA.getAktivnostTip());
		List<Aktivnost> rezerviraneAktivnosti = aktivnostService.findByKorisnickoImeAndVrstaAndDatumDoIsNull(
				korisnik.getKorisnickoIme(), AktivnostVrsta.REZERVACIJA.getAktivnostTip());

		getOsobniPodaci(korisnik, model);

		model.addAttribute("posudeneAktivnosti", posudeneAktivnosti);
		model.addAttribute("rezerviraneAktivnosti", rezerviraneAktivnosti);
		return "profile";
	}

	@RequestMapping(value = "/profileDetalji", method = RequestMethod.GET)
	String showUserProfileDetails(@RequestParam("id") Long id, Model model) {
		Korisnik korisnik = new Korisnik();
		korisnik = korisnikService.findById(id);

		getOsobniPodaci(korisnik, model);

		return "profileDetalji";
	}

	@RequestMapping(value = "/profileEdit", method = RequestMethod.GET)
	String showEditProfile(Model model) {
		CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Korisnik korisnik = korisnikService.findById(user.getUserId());
		List<Zanr> zanrovi = zanrService.findAll();
		List<Drzava> drzave = drzavaService.findAll();
		List<Zanr> korisnikZanrovi = korisnik.getZanrovi();

		for (Zanr zanr : korisnikZanrovi) {
			zanrovi.remove(zanr);
		}

		if (korisnik.getDrzava() != null)
			model.addAttribute("korDrzava", korisnik.getDrzava().getId());
		model.addAttribute("korisnik", korisnik);
		model.addAttribute("korisnikZanrovi", korisnikZanrovi);
		model.addAttribute("zanrovi", zanrovi);
		model.addAttribute("drzave", drzave);

		return "profileEdit";
	}

	@RequestMapping(value = "/profileEdit", method = RequestMethod.POST)
	String updateProfile(Model model, @RequestParam String ime, @RequestParam String prezime,
			@RequestParam String email, @RequestParam String lozinka, @RequestParam String ponovljenaLozinka,
			@RequestParam String adresa, @RequestParam String grad, @RequestParam String postanskiBroj,
			@RequestParam Long novaDrzava, @RequestParam String opis,
			@RequestParam(value = "noviZanrovi", required = false) List<Zanr> noviZanrovi) {

		CustomUserDetails customUser = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Korisnik korisnik = korisnikService.findById(customUser.getUserId());

		Drzava drzava = drzavaService.findOne(novaDrzava);

		if (!lozinka.equals(ponovljenaLozinka)) {
			String nepodudaranje = "Ponovljena lozinka se ne podudara.";
			model.addAttribute("nepodudaranje", nepodudaranje);
		} else {
			if (!lozinka.isEmpty()) {
				korisnik = setKorisnickiPodaci(korisnik, ime, prezime, email, opis, noviZanrovi, adresa, grad, drzava, postanskiBroj);
				korisnik.setLozinka(passwordEncoder.encode(lozinka));
			} else {
				korisnik = setKorisnickiPodaci(korisnik, ime, prezime, email, opis, noviZanrovi, adresa, grad, drzava, postanskiBroj);
			}
			korisnikService.save(korisnik);
			String information = "Uspješno ste ažurirali podatke.";
			model.addAttribute("success", information);
		}

		List<Aktivnost> posudeneAktivnosti = aktivnostService.findByKorisnickoImeAndVrstaAndDatumDoIsNull(
				korisnik.getKorisnickoIme(), AktivnostVrsta.POSUDBA.getAktivnostTip());
		List<Aktivnost> rezerviraneAktivnosti = aktivnostService.findByKorisnickoImeAndVrstaAndDatumDoIsNull(
				korisnik.getKorisnickoIme(), AktivnostVrsta.REZERVACIJA.getAktivnostTip());

		getOsobniPodaci(korisnik, model);

		model.addAttribute("posudeneAktivnosti", posudeneAktivnosti);
		model.addAttribute("rezerviraneAktivnosti", rezerviraneAktivnosti);
		return "profile";
	}

	@RequestMapping(value = "/profileEdit", params = "odustani", method = RequestMethod.POST)
	public String cancelProfileEdit() {
		return "redirect:/profile";
	}

	public void getOsobniPodaci(Korisnik korisnik, Model model) {

		List<PovijestCitanja> wishAktivnosti = povijestCitanjaRepository.findByKorisnikAndVrsta(korisnik,
				PovijestCitanjaVrsta.PLANIRAM.getPovijestCitanjaTip());
		List<Knjiga> wishKnjige = new ArrayList<Knjiga>();

		List<PovijestCitanja> readingAktivnosti = povijestCitanjaRepository.findByKorisnikAndVrsta(korisnik,
				PovijestCitanjaVrsta.CITAM.getPovijestCitanjaTip());
		List<Knjiga> readingKnjige = new ArrayList<Knjiga>();

		List<PovijestCitanja> doneAktivnosti = povijestCitanjaRepository.findByKorisnikAndVrsta(korisnik,
				PovijestCitanjaVrsta.PROCITANO.getPovijestCitanjaTip());
		List<Knjiga> doneKnjige = new ArrayList<Knjiga>();

		List<Komentar> komentariKorKnjige = komentarService
				.findByKorisnikIdAndVijestIdIsNullOrderByDatumObjaveAsc(korisnik.getId());
		List<Komentar> komentariKorVijesti = komentarService
				.findByKorisnikIdAndKnjigaIdIsNullOrderByDatumObjaveAsc(korisnik.getId());
		List<Ocjena> ocjeneKor = ocjenaService.findByKorisnikIdOrderByDatumOcjene(korisnik.getId());
		List<Integer> ocjeneForm = new ArrayList<Integer>();

		for (PovijestCitanja aktivnost : wishAktivnosti) {
			wishKnjige.add(knjigaRepository.findById(aktivnost.getKnjiga().getId()));
		}

		for (PovijestCitanja aktivnost : readingAktivnosti) {
			readingKnjige.add(knjigaRepository.findById(aktivnost.getKnjiga().getId()));
		}

		for (PovijestCitanja aktivnost : doneAktivnosti) {
			doneKnjige.add(knjigaRepository.findById(aktivnost.getKnjiga().getId()));
		}

		String zanrovi = "";
		int i = 0;
		for (Zanr zanr : korisnik.getZanrovi()) {
			if (i++ == korisnik.getZanrovi().size() - 1)
				zanrovi = zanrovi + zanr.getZanr();
			else
				zanrovi = zanrovi + zanr.getZanr() + ", ";
		}

		for (int j = 1; j <= 5; j++) {
			ocjeneForm.add(j);
		}

		model.addAttribute("ocjeneForm", ocjeneForm);
		model.addAttribute("wishKnjige", wishKnjige);
		model.addAttribute("readingKnjige", readingKnjige);
		model.addAttribute("doneKnjige", doneKnjige);
		model.addAttribute("korisnik", korisnik);
		model.addAttribute("zanrovi", zanrovi);
		model.addAttribute("wishAktivnosti", wishAktivnosti);
		model.addAttribute("komentariKorKnjige", komentariKorKnjige);
		model.addAttribute("komentariKorVijesti", komentariKorVijesti);
		model.addAttribute("ocjeneKor", ocjeneKor);
	}
	
	public Korisnik setKorisnickiPodaci(Korisnik korisnik, String ime, String prezime, String email, String opis,
			List<Zanr> noviZanrovi, String adresa, String grad, Drzava drzava, String postanskiBroj) {
		
		korisnik.setIme(ime);
		korisnik.setPrezime(prezime);
		korisnik.setEmail(email);
		korisnik.setOpis(opis);
		korisnik.setZanrovi(noviZanrovi);
		korisnik.setAdresa(adresa);
		korisnik.setGrad(grad);
		korisnik.setDrzava(drzava);
		korisnik.setPostanskiBroj(postanskiBroj);
		
		return korisnik;
	}

}
