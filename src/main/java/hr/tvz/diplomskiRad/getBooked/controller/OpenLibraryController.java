package hr.tvz.diplomskiRad.getBooked.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import hr.tvz.diplomskiRad.getBooked.model.Autor;
import hr.tvz.diplomskiRad.getBooked.model.Isbn;
import hr.tvz.diplomskiRad.getBooked.model.Knjiga;
import hr.tvz.diplomskiRad.getBooked.model.Zanr;
import hr.tvz.diplomskiRad.getBooked.model.ol.OpenLibraryBook;
import hr.tvz.diplomskiRad.getBooked.model.ol.OpenLibraryWrapper;
import hr.tvz.diplomskiRad.getBooked.service.AutorService;
import hr.tvz.diplomskiRad.getBooked.service.IsbnService;
import hr.tvz.diplomskiRad.getBooked.service.KnjigaService;
import hr.tvz.diplomskiRad.getBooked.service.ZanrService;

@Controller
public class OpenLibraryController {
	
	private static final String OPEN_LIBRARY_URL = "https://openlibrary.org/api/books?bibkeys=ISBN:%s&jscmd=details&format=json";
	private static final Logger LOGGER = LoggerFactory.getLogger(OpenLibraryController.class);
	
	@Autowired
	private AutorService autorService;
	
	@Autowired
	private KnjigaService knjigaService;
	
	@Autowired
	private IsbnService isbnService;
	
	@Autowired
	private ZanrService zanrService;

	@RequestMapping(value = "openLibraryForm", method = RequestMethod.GET)
	public String showOpenLibrary(final Model model) {
		return "knjiga/openLibraryForm";
	}
	
	@RequestMapping(value = "getOpenlibraryBook", method = RequestMethod.GET)
	public String openLibraryGet(@RequestParam String iSBN, final Model model, final HttpSession session)
	        throws JsonParseException, JsonMappingException, IOException {

		final RestTemplate restTemplate = new RestTemplate();

		iSBN = StringUtils.trim(iSBN);

		if (StringUtils.isEmpty(iSBN)) {
			model.addAttribute("error", "ISBN nije unesen.");
			return "knjiga/openLibraryForm";
		}

		final String url = String.format(OPEN_LIBRARY_URL, iSBN);

		OpenLibraryWrapper wrapper;
		try {
			final String jsonData = restTemplate.getForObject(url, String.class);
			final ObjectMapper mapper = new ObjectMapper();
			final SimpleModule testModule = new SimpleModule("MyModule", new Version(1, 0, 0, null, null, null))
			        .addDeserializer(OpenLibraryWrapper.class, new OpenLibraryDeserializer());
			mapper.registerModule(testModule);

			wrapper = mapper.readValue(jsonData, OpenLibraryWrapper.class);
		} catch (final Exception e) {
			model.addAttribute("error", "Greška kod dohvaćanja podataka sa servisa Open Library");
			LOGGER.error("Greška kod dohvaćanja podataka sa servisa Open Library za isbn {}", iSBN);
			return "knjiga/openLibraryForm";
		}

		if (wrapper == null) {
			model.addAttribute("error", String.format("Knjiga za ISBN %s ne postoji.", iSBN));
			return "knjiga/openLibraryForm";
		}

		final OpenLibraryBook olBook = wrapper.getOlBook();
		final Knjiga knjiga = new Knjiga();
		String jezik = "";
		String olKey = "";
		String opis = "";
		if(iSBN != null)
			knjiga.setiSBN(iSBN);
		if(olBook.getDetails().getKey() != null) {
			olKey = olBook.getDetails().getKey();
			olKey = olKey.substring(7 , olKey.length()-1);
			knjiga.setOpenLibraryKey(olKey);
		}
		if(olBook.getDetails().getTitle() != null)
			knjiga.setNaziv(olBook.getDetails().getTitle());
		if(olBook.getDetails().getSubjects() != null) {
			opis = olBook.getDetails().getSubjects().toString();
			opis = opis.substring(1, opis.length()-1);
			knjiga.setOpis(opis);
		}
		if(olBook.getDetails().getPublishDate() != null)
			knjiga.setGodinaIzdavanja(olBook.getDetails().getPublishDate());
		if(olBook.getDetails().getLanguages().get(0).getKey() != null) {
			jezik = olBook.getDetails().getLanguages().get(0).getKey().substring(11, 14);
			knjiga.setJezik(jezik);
		}
		if(olBook.getDetails().getNumberOfPages() != null)
			knjiga.setMaterijalniOpis(olBook.getDetails().getNumberOfPages() + " str.");

		final List<String> izdavaci = olBook.getDetails().getPublishers();
		if (izdavaci != null && !izdavaci.isEmpty()) {
			knjiga.setIzdavac(StringUtils.join(izdavaci.toArray(), ", "));
		}
		
		String OLautor = olBook.getDetails().getAuthors().get(0).getName();
		List<Autor> autoriAll = autorService.findAll();
		List<Autor> autori = autorService.findAll();
		List<Autor> autoriChecked = new ArrayList<Autor>();

		if (OLautor != null && !OLautor.isEmpty() && autori != null && !autori.isEmpty()) {
			for(Autor autor : autoriAll)
			{
				if (autor.getImePrezime().equals(OLautor)) {
					autori.remove(autor);
					autoriChecked.add(autor);
					OLautor = "";
				}
			}
		}
		

		model.addAttribute("olautor", OLautor);
		model.addAttribute("autori", autori);
		model.addAttribute("autoriChecked", autoriChecked);
		model.addAttribute("zanrovi", zanrService.findAll());
		model.addAttribute("knjiga", knjiga);
		return "/knjiga/addOpenLibraryKnjiga";
	}
	
	@RequestMapping(value = "/addOpenLibraryKnjiga", params = "odustani", method = RequestMethod.POST)
	public String cancelAddOpenLibraryKnjiga() {
		return "redirect:/openLibraryForm";
	}
	
	@RequestMapping(value = "/addOpenLibraryKnjiga", method = RequestMethod.POST)
	String saveNewKnjiga(@ModelAttribute("knjiga") @Valid final Knjiga knjiga, final BindingResult bindingResult, final Model model, @RequestParam(value = "noviZanrovi", required=false) List<Zanr> noviZanrovi,
			@RequestParam(value = "noviAutori", required=false) List<Autor> noviAutori, @RequestParam String olautor) {
		
		List<Autor> olAutori = new ArrayList<Autor>();
		if (bindingResult.hasErrors()) {
			String error = "Dogodila se pogreška.";
			model.addAttribute("error", error);
			return "knjiga/openLibraryForm";
		}
		
		if(knjigaService.findByISBN(knjiga.getiSBN()) != null) {
			String error = "Postoji knjiga s unesenim ISBN-om.";
			
			model.addAttribute("error", error);
			return "knjiga/openLibraryForm";
		}
		
		if(olautor != null && !olautor.isEmpty() && olautor != "") {
			Autor noviAutor = new Autor();
			noviAutor.setImePrezime(olautor);
			autorService.save(noviAutor);
			olAutori.add(noviAutor);
		}
		else if(noviAutori != null) {
			for(Autor autorCntr : noviAutori)
				olAutori.add(autorCntr);
		}
		
		knjiga.setAutori(olAutori);
		
		knjiga.setZanrovi(noviZanrovi);
		
		knjigaService.save(knjiga);
		String success = "Knjiga uspješno unesena.";
		model.addAttribute("success", success);

		return "knjiga/openLibraryForm";
	}
	
	@RequestMapping(value = "/addOpenLibraryBooks", method = RequestMethod.GET)
	public String openLibraryGetListOfBook(final Model model, final HttpSession session)
	        throws JsonParseException, JsonMappingException, IOException {

		final RestTemplate restTemplate = new RestTemplate();

		List<Knjiga> olKnjige = new ArrayList<Knjiga>();
		List<Autor> olAutori = new ArrayList<Autor>();
		List<Autor> autori = autorService.findAll();
		List<Isbn> iSBNList = isbnService.findAll();
		List<String> iSBNs = new ArrayList<String>();
		for(Isbn isbn : iSBNList) {
			iSBNs.add(isbn.getiSBN());
		}
	
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
		for(String iSBN : iSBNs)
		{
			iSBN = StringUtils.trim(iSBN);

			if (StringUtils.isEmpty(iSBN)) {
				model.addAttribute("greska", "ISBN nije unesen.");
				return "knjiga/listaKnjiga";
			}

			final String url = String.format(OPEN_LIBRARY_URL, iSBN);

			OpenLibraryWrapper wrapper;
			try {
				final String jsonData = restTemplate.getForObject(url, String.class);
				final ObjectMapper mapper = new ObjectMapper();
				final SimpleModule testModule = new SimpleModule("MyModule", new Version(1, 0, 0, null, null, null))
				        .addDeserializer(OpenLibraryWrapper.class, new OpenLibraryDeserializer());
				mapper.registerModule(testModule);

				wrapper = mapper.readValue(jsonData, OpenLibraryWrapper.class);
			} catch (final Exception e) {
				model.addAttribute("greska", "Greška kod dohvaćanja podataka sa servisa Open Library");
				LOGGER.error("Greška kod dohvaćanja podataka sa servisa Open Library za isbn {}", iSBN);
				return "knjiga/listaKnjiga";
			}

			if (wrapper == null) {
				model.addAttribute("greska", String.format("Knjiga za ISBN %s ne postoji.", iSBN));
				return "knjiga/listaKnjiga";
			}

			final OpenLibraryBook olBook = wrapper.getOlBook();
			
			String OLautor = olBook.getDetails().getAuthors().get(0).getName();
			
			Autor autor = new Autor();
			autor.setImePrezime(OLautor);
			
			olAutori.add(autor);
		}
		
		for(Autor autor : olAutori) {
			if(autorService.findByImePrezime(autor.getImePrezime()).size() < 1)
				autorService.save(autor);
		}
		
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

		autori = autorService.findAll();
		
		for(String iSBN : iSBNs)
		{
			iSBN = StringUtils.trim(iSBN);

			if (StringUtils.isEmpty(iSBN)) {
				model.addAttribute("greska", "ISBN nije unesen.");
				return "knjiga/listaKnjiga";
			}

			final String url = String.format(OPEN_LIBRARY_URL, iSBN);

			OpenLibraryWrapper wrapper;
			try {
				final String jsonData = restTemplate.getForObject(url, String.class);
				final ObjectMapper mapper = new ObjectMapper();
				final SimpleModule testModule = new SimpleModule("MyModule", new Version(1, 0, 0, null, null, null))
				        .addDeserializer(OpenLibraryWrapper.class, new OpenLibraryDeserializer());
				mapper.registerModule(testModule);

				wrapper = mapper.readValue(jsonData, OpenLibraryWrapper.class);
			} catch (final Exception e) {
				model.addAttribute("greska", "Greška kod dohvaćanja podataka sa servisa Open Library");
				LOGGER.error("Greška kod dohvaćanja podataka sa servisa Open Library za isbn {}", iSBN);
				return "knjiga/listaKnjiga";
			}

			if (wrapper == null) {
				model.addAttribute("greska", String.format("Knjiga za ISBN %s ne postoji.", iSBN));
				return "knjiga/listaKnjiga";
			}

			final OpenLibraryBook olBook = wrapper.getOlBook();
			final Knjiga knjiga = new Knjiga();
			String jezik = "";
			String olKey = "";
			String opis = "";
			if(iSBN != null)
				knjiga.setiSBN(iSBN);
			if(olBook.getDetails().getKey() != null);
			olKey = olBook.getDetails().getKey();
			olKey = olKey.substring(7 , olKey.length()-1);
			knjiga.setOpenLibraryKey(olKey);
			if(olBook.getDetails().getTitle() != null)
				knjiga.setNaziv(olBook.getDetails().getTitle());
			if(olBook.getDetails().getSubjects() != null) {
				opis = olBook.getDetails().getSubjects().toString();
				opis = opis.substring(1, opis.length()-1);
				knjiga.setOpis(opis);
			}
			if(olBook.getDetails().getPublishDate() != null)
				knjiga.setGodinaIzdavanja(olBook.getDetails().getPublishDate());
			if(olBook.getDetails().getLanguages().get(0).getKey() != null) {
				jezik = olBook.getDetails().getLanguages().get(0).getKey().substring(11, 14);
				knjiga.setJezik(jezik);
			}
			if(olBook.getDetails().getNumberOfPages() != null)
				knjiga.setMaterijalniOpis(olBook.getDetails().getNumberOfPages() + " str.");

			final List<String> izdavaci = olBook.getDetails().getPublishers();
			if (izdavaci != null && !izdavaci.isEmpty()) {
				knjiga.setIzdavac(StringUtils.join(izdavaci.toArray(), ", "));
			}
			
			String OLautor = olBook.getDetails().getAuthors().get(0).getName();
			List<Autor> noviAutori = new ArrayList<Autor>();

			if (OLautor != null && !OLautor.isEmpty() && autori != null && !autori.isEmpty()) {
				for(Autor autor : autori)
				{
					if (autor.getImePrezime().equals(OLautor)) {
						noviAutori.add(autor);
						OLautor = "";
					}
				}
			}
			
			if(OLautor != "")
			{
				Autor noviAutor = new Autor();
				noviAutor.setImePrezime(OLautor);
				noviAutori.add(noviAutor);
				olAutori.add(noviAutor);
			}
			knjiga.setAutori(noviAutori);
			olKnjige.add(knjiga);
		}

		for(Knjiga knjiga : olKnjige) {
			knjigaService.save(knjiga);
		}
		String success = "Knjige uspješno unesene.";
		List<Knjiga> listaKnjiga = knjigaService.findAll();
		model.addAttribute("success", success);
		model.addAttribute("knjige", listaKnjiga);

		return "knjiga/listaKnjiga";
	}

}
