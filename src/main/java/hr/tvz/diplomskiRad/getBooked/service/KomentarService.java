package hr.tvz.diplomskiRad.getBooked.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.tvz.diplomskiRad.getBooked.dao.KomentarRepository;
import hr.tvz.diplomskiRad.getBooked.model.Komentar;

@Service
public class KomentarService {
	
	@Autowired
	private KomentarRepository komentarRepository;

	public List<Komentar> findByKorisnikIdOrderByDatumObjaveAsc(Long id){
		return komentarRepository.findByKorisnikIdOrderByDatumObjaveAsc(id);
	}
	
	public List<Komentar> findByKorisnikIdAndVijestIdIsNullOrderByDatumObjaveAsc(Long id){
		return komentarRepository.findByKorisnikIdAndVijestIdIsNullOrderByDatumObjaveAsc(id);
	}
	
	public List<Komentar> findByKorisnikIdAndKnjigaIdIsNullOrderByDatumObjaveAsc(Long id){
		return komentarRepository.findByKorisnikIdAndKnjigaIdIsNullOrderByDatumObjaveAsc(id);
	}
}

