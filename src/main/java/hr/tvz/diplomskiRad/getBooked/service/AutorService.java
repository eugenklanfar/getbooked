package hr.tvz.diplomskiRad.getBooked.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.tvz.diplomskiRad.getBooked.dao.AutorRepository;
import hr.tvz.diplomskiRad.getBooked.model.Autor;

@Service
public class AutorService {

	@Autowired
	private AutorRepository autorRepository;
	
	public List<Autor> findAll() {
		return autorRepository.findAll();
	}
	
	public Autor findById(Long id) {
		return autorRepository.findById(id);
	}
	
	public List<Autor> findByKnjigeId(Long knjigaId) {
		return autorRepository.findByKnjigeId(knjigaId);
	}
	
	public List<Autor> findByImePrezime(String imePrezime) {
		return autorRepository.findByImePrezime(imePrezime);
	}
	
	public Autor save(Autor autor) {
		return autorRepository.save(autor);
	}
}
