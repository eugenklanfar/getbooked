package hr.tvz.diplomskiRad.getBooked.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.tvz.diplomskiRad.getBooked.dao.PovijestCitanjaRepository;
import hr.tvz.diplomskiRad.getBooked.dao.KorisnikRepository;
import hr.tvz.diplomskiRad.getBooked.model.PovijestCitanja;
import hr.tvz.diplomskiRad.getBooked.model.Knjiga;
import hr.tvz.diplomskiRad.getBooked.model.Korisnik;

@Service
public class PovijestCitanjaService {

	@Autowired
	private PovijestCitanjaRepository aktivnostRepository;
	
	@Autowired
	private KorisnikRepository korisnikRepository;
	
	public void save(PovijestCitanja aktivnost) {
		Korisnik korisnik = korisnikRepository.findById(aktivnost.getKorisnik().getId());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		String zadnjaAktivnost = dateFormat.format(date);
		korisnik.setZadnjaAktivnost(zadnjaAktivnost);
		korisnikRepository.save(korisnik);
		
		aktivnostRepository.save(aktivnost);
	}
	
	public PovijestCitanja findByKorisnikAndKnjiga(Korisnik korisnik, Knjiga knjiga) {
		return aktivnostRepository.findByKorisnikAndKnjiga(korisnik, knjiga);
	}
}
