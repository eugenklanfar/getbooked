package hr.tvz.diplomskiRad.getBooked.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.tvz.diplomskiRad.getBooked.dao.KorisnikRepository;
import hr.tvz.diplomskiRad.getBooked.model.Korisnik;

@Service
public class KorisnikService {
	
	@Autowired
	private KorisnikRepository korisnikRepository;
	
	public void save(Korisnik korisnik) {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		String zadnjaAktivnost = dateFormat.format(date);
		korisnik.setZadnjaAktivnost(zadnjaAktivnost);
		korisnikRepository.save(korisnik);
	}
	
	public List<Korisnik> findAll() {
		return korisnikRepository.findAll();
	}
	
	public Korisnik findById(Long id) {
		return korisnikRepository.findById(id);
	}
	

	public Korisnik findByKorisnickoIme(String korisnickoIme) {
		return korisnikRepository.findByKorisnickoIme(korisnickoIme);
	}

}
