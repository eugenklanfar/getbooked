package hr.tvz.diplomskiRad.getBooked.service;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

/**
 * Klasa za dobivanje dodatnih podataka o trenutno prijavljenom korisniku.
 * @author eugen
 *
 */
public class CustomUserDetails extends org.springframework.security.core.userdetails.User{
	
	private static final long serialVersionUID = 1L;
	long userId;
	public CustomUserDetails (String username, String password,long userId, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked, Collection<GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, authorities);
		this.userId = userId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}

}
