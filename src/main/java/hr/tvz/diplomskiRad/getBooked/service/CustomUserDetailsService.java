package hr.tvz.diplomskiRad.getBooked.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.tvz.diplomskiRad.getBooked.model.Korisnik;

/**
 * Korisnicki servis koristen pri autentikaciji i autorizaciji korisnika
 * 
 * @author eugen
 *
 */
@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private KorisnikService korisnikService;

	/**
	 * Pretraga korisnika prema korisnickom imenu u svrhu prijave u sustav
	 */
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(final String korisnickoIme) throws UsernameNotFoundException {
		final Korisnik korisnik = korisnikService.findByKorisnickoIme(korisnickoIme);
		if (korisnik == null)
			throw new UsernameNotFoundException("Korisnik nije pronađen");

		CustomUserDetails userDetails = new CustomUserDetails(korisnik.getKorisnickoIme(), korisnik.getLozinka(),
				korisnik.getId(), korisnik.getAktivan().equals(true), true, true, true,
				getGrantedAuthorities(korisnik));

		return userDetails;
	}

	private List<GrantedAuthority> getGrantedAuthorities(final Korisnik korisnik) {
		final List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		if (korisnik.getAdministrator().equals(true))
			authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		else
			authorities.add(new SimpleGrantedAuthority("ROLE_KORISNIK"));
		return authorities;
	}
}
