package hr.tvz.diplomskiRad.getBooked.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.tvz.diplomskiRad.getBooked.dao.OcjenaRepository;
import hr.tvz.diplomskiRad.getBooked.model.Ocjena;

@Service
public class OcjenaService {
	
	@Autowired
	private OcjenaRepository ocjenaRepository;
	
	public List<Ocjena> findByKorisnikIdOrderByDatumOcjene(Long id) {
		return ocjenaRepository.findByKorisnikIdOrderByDatumOcjene(id);
	}

}
