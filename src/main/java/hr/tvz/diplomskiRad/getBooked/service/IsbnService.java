package hr.tvz.diplomskiRad.getBooked.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.tvz.diplomskiRad.getBooked.dao.IsbnRepository;
import hr.tvz.diplomskiRad.getBooked.model.Isbn;

@Service
public class IsbnService {
	
	@Autowired
	private IsbnRepository isbnRepository;
	
	public List<Isbn> findAll() {
		return isbnRepository.findAll();
	}

}
