package hr.tvz.diplomskiRad.getBooked.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.tvz.diplomskiRad.getBooked.dao.DrzavaRepository;
import hr.tvz.diplomskiRad.getBooked.model.Drzava;

@Service
public class DrzavaService {
	
	@Autowired
	private DrzavaRepository drzavaRepository;
	
	public List<Drzava> findAll() {
		return drzavaRepository.findAll();
	}
	
	public Drzava findOne(Long id) {
		return drzavaRepository.findOne(id);
	}

}
