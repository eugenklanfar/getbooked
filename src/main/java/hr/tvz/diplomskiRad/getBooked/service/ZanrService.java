package hr.tvz.diplomskiRad.getBooked.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.tvz.diplomskiRad.getBooked.dao.ZanrRepository;
import hr.tvz.diplomskiRad.getBooked.model.Zanr;

@Service
public class ZanrService {

	@Autowired
	private ZanrRepository zanrRepository;
	
	public List<Zanr> findAll() {
		return zanrRepository.findAll();
	}
	
}
