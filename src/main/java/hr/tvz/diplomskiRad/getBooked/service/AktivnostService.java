package hr.tvz.diplomskiRad.getBooked.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.tvz.diplomskiRad.getBooked.dao.AktivnostRepository;
import hr.tvz.diplomskiRad.getBooked.model.Aktivnost;
import hr.tvz.diplomskiRad.getBooked.model.AktivnostVrsta;
import hr.tvz.diplomskiRad.getBooked.model.Primjerak;
import hr.tvz.diplomskiRad.getBooked.model.Status;


@Service
public class AktivnostService {
	
	@Autowired
	private AktivnostRepository aktivnostRepository;

	@Autowired
	private PrimjerakService primjerakService;
	
	@Transactional(rollbackFor = Exception.class)
	public Aktivnost saveRezervacija(final Aktivnost rezervacija) {

		Primjerak primjerak = rezervacija.getPrimjerak();
		primjerak.setStatus(Status.REZERVIRAN.getStatus());
		primjerakService.save(primjerak);

		return aktivnostRepository.save(rezervacija);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void savePosudbaFromRezervacija(final Aktivnost posudba, final Long id) {

		final Aktivnost rezervacija = aktivnostRepository.findOne(id);
		rezervacija.setDatumDo(posudba.getDatumOd());
		aktivnostRepository.save(rezervacija);

		rezervacija.getPrimjerak().setStatus(Status.POSUDEN.getStatus());
		primjerakService.save(rezervacija.getPrimjerak());

		posudba.setVrsta(AktivnostVrsta.POSUDBA.getAktivnostTip());
		posudba.setKorisnickoIme(rezervacija.getKorisnickoIme());
		posudba.setPrimjerak(rezervacija.getPrimjerak());

		aktivnostRepository.save(posudba);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Aktivnost zatvaranjeRezervacije(final Aktivnost rezervacija) {

		rezervacija.setStatus(Status.PASIVAN.getStatus());
		rezervacija.setDatumDo(new Date());
		aktivnostRepository.save(rezervacija);

		final Primjerak primjerak = primjerakService.findOne(rezervacija.getPrimjerak().getId());
		primjerak.setStatus(Status.AKTIVAN.getStatus());
		primjerakService.save(primjerak);

		return rezervacija;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Aktivnost zatvaranjePosudbe(final Long idPosudbe, final Date datumDo) {

		final Aktivnost aktivnostIzBaze = aktivnostRepository.findOne(idPosudbe);
		aktivnostIzBaze.setDatumDo(datumDo);
		aktivnostRepository.save(aktivnostIzBaze);

		final Primjerak primjerak = aktivnostIzBaze.getPrimjerak();
		primjerak.setStatus(Status.AKTIVAN.getStatus());
		primjerakService.save(primjerak);

		return aktivnostIzBaze;
	}
	
	public Aktivnost getAktivnostById(final Long id) {
		return aktivnostRepository.findOne(id);
	}
	
	public Aktivnost findOne(final Long id) {
		return aktivnostRepository.findOne(id);
	}
	
	public List<Aktivnost> findByKorisnickoImeAndVrsta(String username, String aktivnostTip) {
		return aktivnostRepository.findByKorisnickoImeAndVrsta(username, aktivnostTip);
	}
	
	public List<Aktivnost> findByKorisnickoImeAndVrstaAndDatumDoIsNull(String username, String aktivnostTip) {
		return aktivnostRepository.findByKorisnickoImeAndVrstaAndDatumDoIsNull(username, aktivnostTip);
	}

	public List<Aktivnost> findByVrstaAndDatumDoIsNull(String vrsta) {
		return aktivnostRepository.findByVrstaAndDatumDoIsNull(vrsta);
	}

}
