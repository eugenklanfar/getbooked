package hr.tvz.diplomskiRad.getBooked.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.tvz.diplomskiRad.getBooked.dao.PrimjerakRepository;
import hr.tvz.diplomskiRad.getBooked.model.Primjerak;

@Service
public class PrimjerakService {
	
	@Autowired
	private PrimjerakRepository primjerakRepository;
	
	public Primjerak findById(Long id) {
		return primjerakRepository.findById(id);
	}
	
	public void save(Primjerak primjerak) {
		primjerakRepository.save(primjerak);
	}
	
	public Primjerak findOne(Long id) {
		return primjerakRepository.findOne(id);
	}

}
