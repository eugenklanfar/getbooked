package hr.tvz.diplomskiRad.getBooked.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.tvz.diplomskiRad.getBooked.dao.KnjigaRepository;
import hr.tvz.diplomskiRad.getBooked.model.Knjiga;

@Service
public class KnjigaService {
	
	@Autowired
	private KnjigaRepository knjigaRepository;

	public List<Knjiga> findAll() {
		return knjigaRepository.findAll();
	}
	
	public List<Knjiga> findByOrderByNazivAsc() {
		return knjigaRepository.findByOrderByNazivAsc();
	} 

	public Knjiga findById(Long id) {
		return knjigaRepository.findById(id);
	}
	
	public Knjiga findByISBN(String ISBN) {
		return knjigaRepository.findByISBN(ISBN);
	}
	
	public void save(Knjiga knjiga) {
		knjigaRepository.save(knjiga);
	}
	
	public void saveAndFlush(Knjiga knjiga) {
		knjigaRepository.saveAndFlush(knjiga);
	}
	
}
