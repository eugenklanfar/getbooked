package hr.tvz.diplomskiRad.getBooked.model.ol;

public class OpenLibraryWrapper {

	private OpenLibraryBook olBook;

	public OpenLibraryWrapper(final OpenLibraryBook olBook) {
		this.olBook = olBook;
	}

	public OpenLibraryBook getOlBook() {
		return olBook;
	}

	public void setOlBook(final OpenLibraryBook olBook) {
		this.olBook = olBook;
	}

}
