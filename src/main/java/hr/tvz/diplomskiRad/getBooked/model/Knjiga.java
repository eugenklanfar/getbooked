package hr.tvz.diplomskiRad.getBooked.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Model za knjigu.
 * 
 * @author eugen
 *
 */
@Entity
@Table(name = "Knjiga")
public class Knjiga {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotBlank
	@Size(min = 2, max = 30, message = "ISBN mora sadržavati između 2 do 20 znakova.")
	@Column(name = "iSBN", unique = true, nullable = false)
	private String iSBN;

	@NotBlank
	@Size(min = 2, max = 150, message = "Naziv mora sadržavati između 2 do 50 znakova.")
	@Column(name = "naziv", unique = false, nullable = false)
	private String naziv;

	@Size(max = 1000)
	@Column(name = "opis", unique = false, nullable = true)
	private String opis;

	// materijalni opis 443 str. : zemljop. crteži ; 21 cm
	@Size(max = 200)
	@Column(name = "materijalni_opis", unique = false, nullable = true)
	private String materijalniOpis;

	@NotBlank
	@Size(min = 2, max = 50)
	@Column(name = "godina_izdavanja", nullable = false)
	private String godinaIzdavanja;

	@Size(max = 50)
	@Column(name = "jezik", nullable = true)
	private String jezik;

	@Size(max = 100)
	@Column(name = "izdavac", nullable = true)
	private String izdavac;

	@Size(max = 100)
	@Column(name = "open_library_key", nullable = true)
	private String openLibraryKey;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "knjiga_autor", joinColumns = @JoinColumn(name = "knjiga_id"), inverseJoinColumns = @JoinColumn(name = "autor_id"))
	private List<Autor> autori;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "knjiga_zanr", joinColumns = @JoinColumn(name = "knjiga_id"), inverseJoinColumns = @JoinColumn(name = "zanr_id"))
	private List<Zanr> zanrovi;

	@OneToMany(mappedBy = "knjiga")
	private Set<Komentar> komentari;

	@OneToMany(mappedBy = "knjiga")
	private Set<Ocjena> ocjene;

	@OneToMany(mappedBy = "knjiga")
	private Set<PovijestCitanja> povijestCitanja;

	@OneToMany(mappedBy = "knjiga")
	private Set<Primjerak> primjerci;

	@Transient
	private Long noviAutor;

	@Transient
	private Float prosjecnaOcjena;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getiSBN() {
		return iSBN;
	}

	public void setiSBN(String iSBN) {
		this.iSBN = iSBN;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getMaterijalniOpis() {
		return materijalniOpis;
	}

	public void setMaterijalniOpis(String materijalniOpis) {
		this.materijalniOpis = materijalniOpis;
	}

	public String getGodinaIzdavanja() {
		return godinaIzdavanja;
	}

	public void setGodinaIzdavanja(String godinaIzdavanja) {
		this.godinaIzdavanja = godinaIzdavanja;
	}

	public String getJezik() {
		return jezik;
	}

	public void setJezik(String jezik) {
		this.jezik = jezik;
	}

	public String getIzdavac() {
		return izdavac;
	}

	public void setIzdavac(String izdavac) {
		this.izdavac = izdavac;
	}

	public String getOpenLibraryKey() {
		return openLibraryKey;
	}

	public void setOpenLibraryKey(String openLibraryKey) {
		this.openLibraryKey = openLibraryKey;
	}

	public List<Autor> getAutori() {
		return autori;
	}

	public void setAutori(List<Autor> autori) {
		this.autori = autori;
	}

	public Long getNoviAutor() {
		return noviAutor;
	}

	public void setNoviAutor(Long noviAutor) {
		this.noviAutor = noviAutor;
	}

	public Set<Komentar> getKomentari() {
		return komentari;
	}

	public void setKomentari(Set<Komentar> komentari) {
		this.komentari = komentari;
	}

	public Set<Ocjena> getOcjene() {
		return ocjene;
	}

	public void setOcjene(Set<Ocjena> ocjene) {
		this.ocjene = ocjene;
	}

	public Set<PovijestCitanja> getPovijestCitanja() {
		return povijestCitanja;
	}

	public void setPovijestCitanja(Set<PovijestCitanja> aktivnost) {
		this.povijestCitanja = aktivnost;
	}

	public List<Zanr> getZanrovi() {
		return zanrovi;
	}

	public void setZanrovi(List<Zanr> zanrovi) {
		this.zanrovi = zanrovi;
	}

	public Set<Primjerak> getPrimjerci() {
		return primjerci;
	}

	public void setPrimjerci(Set<Primjerak> primjerci) {
		this.primjerci = primjerci;
	}

	public Float getProsjecnaOcjena() {
		return prosjecnaOcjena;
	}

	public void setProsjecnaOcjena(Float prosjecnaOcjena) {
		this.prosjecnaOcjena = prosjecnaOcjena;
	}

}
