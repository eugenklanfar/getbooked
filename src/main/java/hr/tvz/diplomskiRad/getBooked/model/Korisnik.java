package hr.tvz.diplomskiRad.getBooked.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Model za korisnika koji se prijavljuje u sustav kao običan korisnik ili admin.
 * @author eugen
 *
 */
@Entity
@Table(name = "Korisnik")
public class Korisnik {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank(message="")
	@Size(min=3, max=25, message="Korisničko ime mora sadržavati između 3 do 25 znakova.")
	@Column(name = "korisnicko_ime", unique = true, nullable = false)
	private String korisnickoIme;

	@NotBlank(message="")
	@Column(name = "lozinka", nullable = false)
	private String lozinka;
	
	@NotBlank(message="")
	@Size(min = 2, max = 30, message="Ime mora sadržavati između 2 do 30 znakova.")
	@Column(name = "ime", nullable = false)
	private String ime;

	@NotBlank(message="")
	@Size(min = 2, max = 50, message="Prezime mora sadržavati između 2 do 50 znakova.")
	@Column(name = "prezime", nullable = false)
	private String prezime;

	@NotBlank(message="")
	@Size(min = 2, max = 200)
	@Column(name = "email", nullable = false)
	private String email;
	
	@Column(name = "adresa", nullable = true)
	private String adresa;
	
	@Column(name = "grad", nullable = true)
	private String grad;
	
	@Column(name = "postanskiBroj", nullable = true)
	private String postanskiBroj;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "drzava_id")
	private Drzava drzava;
	
	@NotNull
	@Column(name ="aktivan", nullable = false)
	private Boolean aktivan = true;

	@NotNull
	@Column(name = "administrator", nullable = false)
	private Boolean administrator = false;
	
	@Size(min = 2, max = 50)
	@Column(name = "zadnja_aktivnost", nullable = true)
	private String zadnjaAktivnost;
	
	@Size(max = 1000)
	@Column(name = "opis", unique = false, nullable = true)
	private String opis;
	
	@OneToMany(mappedBy = "korisnik")
	private Set<Komentar> komentari;

	@OneToMany(mappedBy = "korisnik")
	private Set<Ocjena> ocjene;
	
	@OneToMany(mappedBy = "korisnik")
	private Set<PovijestCitanja> povijestCitanja;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "korisnik_zanr", joinColumns = @JoinColumn(name = "korisnik_id"), inverseJoinColumns = @JoinColumn(name = "zanr_id"))
	private List<Zanr> zanrovi;
	
	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getAktivan() {
		return aktivan;
	}

	public void setAktivan(Boolean aktivan) {
		this.aktivan = aktivan;
	}

	public Boolean getAdministrator() {
		return administrator;
	}

	public void setAdministrator(Boolean administrator) {
		this.administrator = administrator;
	}

	public Set<Komentar> getKomentari() {
		return komentari;
	}

	public void setKomentari(Set<Komentar> komentari) {
		this.komentari = komentari;
	}

	public Set<Ocjena> getOcjene() {
		return ocjene;
	}

	public void setOcjene(Set<Ocjena> ocjene) {
		this.ocjene = ocjene;
	}

	public Set<PovijestCitanja> getPovijestCitanja() {
		return povijestCitanja;
	}

	public void setPovijestCitanja(Set<PovijestCitanja> aktivnosti) {
		this.povijestCitanja = aktivnosti;
	}

	public String getZadnjaAktivnost() {
		return zadnjaAktivnost;
	}

	public void setZadnjaAktivnost(String zadnjaAktivnost) {
		this.zadnjaAktivnost = zadnjaAktivnost;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public List<Zanr> getZanrovi() {
		return zanrovi;
	}

	public void setZanrovi(List<Zanr> zanrovi) {
		this.zanrovi = zanrovi;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getGrad() {
		return grad;
	}

	public void setGrad(String grad) {
		this.grad = grad;
	}

	public String getPostanskiBroj() {
		return postanskiBroj;
	}

	public void setPostanskiBroj(String postanskiBroj) {
		this.postanskiBroj = postanskiBroj;
	}

	public Drzava getDrzava() {
		return drzava;
	}

	public void setDrzava(Drzava drzava) {
		this.drzava = drzava;
	}
	
}
