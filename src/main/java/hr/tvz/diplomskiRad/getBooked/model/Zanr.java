package hr.tvz.diplomskiRad.getBooked.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "Zanr")
public class Zanr {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank(message = "")
	@Column(name = "zanr", nullable = false)
	private String zanr;

	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "zanrovi")
	private Set<Korisnik> korisnici;

	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "zanrovi")
	private Set<Knjiga> knjige;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getZanr() {
		return zanr;
	}

	public void setZanr(String zanr) {
		this.zanr = zanr;
	}

	public Set<Korisnik> getKorisnici() {
		return korisnici;
	}

	public void setKorisnici(Set<Korisnik> korisnici) {
		this.korisnici = korisnici;
	}

	public Set<Knjiga> getKnjige() {
		return knjige;
	}

	public void setKnjige(Set<Knjiga> knjige) {
		this.knjige = knjige;
	}
}
