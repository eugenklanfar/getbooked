package hr.tvz.diplomskiRad.getBooked.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Model za popis isbn knjiga.
 * @author eugen
 *
 */

@Entity
@Table(name = "Isbn")
public class Isbn {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank
	@Size(min = 2, max = 20, message="ISBN mora sadržavati između 2 do 20 znakova.")
	@Column(name = "iSBN", unique = true, nullable = false)
	private String iSBN;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getiSBN() {
		return iSBN;
	}

	public void setiSBN(String iSBN) {
		this.iSBN = iSBN;
	}
	
	
}
