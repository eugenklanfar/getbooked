package hr.tvz.diplomskiRad.getBooked.model.ol;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "level", "type", "title" })
public class TableOfContent {

	@JsonProperty("level")
	private Integer level;
	@JsonProperty("type")
	private Type type;
	@JsonProperty("title")
	private String title;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 *
	 * @return The level
	 */
	@JsonProperty("level")
	public Integer getLevel() {
		return level;
	}

	/**
	 *
	 * @param level
	 *            The level
	 */
	@JsonProperty("level")
	public void setLevel(Integer level) {
		this.level = level;
	}

	/**
	 *
	 * @return The type
	 */
	@JsonProperty("type")
	public Type getType() {
		return type;
	}

	/**
	 *
	 * @param type
	 *            The type
	 */
	@JsonProperty("type")
	public void setType(Type type) {
		this.type = type;
	}

	/**
	 *
	 * @return The title
	 */
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	/**
	 *
	 * @param title
	 *            The title
	 */
	@JsonProperty("title")
	public void setTitle(String title) {
		this.title = title;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
