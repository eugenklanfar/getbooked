package hr.tvz.diplomskiRad.getBooked.model;

public enum AktivnostVrsta {
	REZERVACIJA("REZERVACIJA"), POSUDBA("POSUDBA");

	String aktivnostTip;

	private AktivnostVrsta(final String aktivnostTip) {
		this.aktivnostTip = aktivnostTip;
	}

	public String getAktivnostTip() {
		return aktivnostTip;
	}

}
