package hr.tvz.diplomskiRad.getBooked.model;

public enum Status {

	AKTIVAN("Aktivan"), PASIVAN("Pasivan"), REZERVIRAN("Rezerviran"), POSUDEN("Posuđen");

	private String status;

	private Status(final String status) {
		this.status = status;
	}

	public String getStatus() {
		return this.status;
	}

	@Override
	public String toString() {
		return this.status;
	}

	public String getName() {
		return this.name();
	}
}
