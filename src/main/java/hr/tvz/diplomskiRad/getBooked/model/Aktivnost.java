package hr.tvz.diplomskiRad.getBooked.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import hr.tvz.diplomskiRad.getBooked.model.AktivnostVrsta;
import hr.tvz.diplomskiRad.getBooked.model.Korisnik;
import hr.tvz.diplomskiRad.getBooked.model.Primjerak;
import hr.tvz.diplomskiRad.getBooked.model.Status;

@Entity
@Table(name = "Aktivnost")
public class Aktivnost {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@Column(name = "datum_od")
	private Date datumOd;

	@Column(name = "datum_do")
	private Date datumDo;

	@NotNull
	@Column(name = "datum_isteka")
	private Date datumIsteka;

	@Column(name = "vrsta")
	private String vrsta = AktivnostVrsta.REZERVACIJA.getAktivnostTip();

	@Column(name = "korisnicko_ime")
	private String korisnickoIme;

	@Transient
	private Korisnik korisnik;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "primjerak_id")
	private Primjerak primjerak;

	@NotEmpty
	@Column(name = "status", nullable = false)
	private String status = Status.AKTIVAN.getStatus();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDatumOd() {
		return datumOd;
	}

	public void setDatumOd(Date datumOd) {
		this.datumOd = datumOd;
	}

	public Date getDatumDo() {
		return datumDo;
	}

	public void setDatumDo(Date datumDo) {
		this.datumDo = datumDo;
	}

	public Date getDatumIsteka() {
		return datumIsteka;
	}

	public void setDatumIsteka(Date datumIsteka) {
		this.datumIsteka = datumIsteka;
	}

	public String getVrsta() {
		return vrsta;
	}

	public void setVrsta(String vrsta) {
		this.vrsta = vrsta;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Primjerak getPrimjerak() {
		return primjerak;
	}

	public void setPrimjerak(Primjerak primjerak) {
		this.primjerak = primjerak;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public boolean isAktivan() {
		return status.equals(Status.AKTIVAN.getStatus());
	}

}
