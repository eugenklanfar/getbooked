package hr.tvz.diplomskiRad.getBooked.model.ol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "amazon", "google", "goodreads", "librarything" })
public class Identifiers {

	@JsonProperty("amazon")
	private List<String> amazon = new ArrayList<String>();
	@JsonProperty("google")
	private List<String> google = new ArrayList<String>();
	@JsonProperty("goodreads")
	private List<String> goodreads = new ArrayList<String>();
	@JsonProperty("librarything")
	private List<String> librarything = new ArrayList<String>();
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 *
	 * @return The amazon
	 */
	@JsonProperty("amazon")
	public List<String> getAmazon() {
		return amazon;
	}

	/**
	 *
	 * @param amazon
	 *            The amazon
	 */
	@JsonProperty("amazon")
	public void setAmazon(List<String> amazon) {
		this.amazon = amazon;
	}

	/**
	 *
	 * @return The google
	 */
	@JsonProperty("google")
	public List<String> getGoogle() {
		return google;
	}

	/**
	 *
	 * @param google
	 *            The google
	 */
	@JsonProperty("google")
	public void setGoogle(List<String> google) {
		this.google = google;
	}

	/**
	 *
	 * @return The goodreads
	 */
	@JsonProperty("goodreads")
	public List<String> getGoodreads() {
		return goodreads;
	}

	/**
	 *
	 * @param goodreads
	 *            The goodreads
	 */
	@JsonProperty("goodreads")
	public void setGoodreads(List<String> goodreads) {
		this.goodreads = goodreads;
	}

	/**
	 *
	 * @return The librarything
	 */
	@JsonProperty("librarything")
	public List<String> getLibrarything() {
		return librarything;
	}

	/**
	 *
	 * @param librarything
	 *            The librarything
	 */
	@JsonProperty("librarything")
	public void setLibrarything(List<String> librarything) {
		this.librarything = librarything;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
