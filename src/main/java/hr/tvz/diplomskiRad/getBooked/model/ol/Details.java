package hr.tvz.diplomskiRad.getBooked.model.ol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "number_of_pages", "table_of_contents", "weight", "covers", "lc_classifications",
        "latest_revision", "ocaid", "contributors", "source_records", "title", "languages", "subjects",
        "publish_country", "by_statement", "oclc_numbers", "type", "physical_dimensions", "revision", "publishers",
        "description", "physical_format", "last_modified", "key", "authors", "publish_places", "pagination",
        "classifications", "created", "lccn", "notes", "identifiers", "isbn_13", "dewey_decimal_class", "isbn_10",
        "publish_date", "works" })
public class Details {

	@JsonProperty("number_of_pages")
	private Integer numberOfPages;
	@JsonProperty("table_of_contents")
	private List<TableOfContent> tableOfContents = new ArrayList<TableOfContent>();
	@JsonProperty("weight")
	private String weight;
	@JsonProperty("covers")
	private List<Integer> covers = new ArrayList<Integer>();
	@JsonProperty("lc_classifications")
	private List<String> lcClassifications = new ArrayList<String>();
	@JsonProperty("latest_revision")
	private Integer latestRevision;
	@JsonProperty("ocaid")
	private String ocaid;
	@JsonProperty("contributors")
	private List<Contributor> contributors = new ArrayList<Contributor>();
	@JsonProperty("source_records")
	private List<String> sourceRecords = new ArrayList<String>();
	@JsonProperty("title")
	private String title;
	@JsonProperty("languages")
	private List<Language> languages = new ArrayList<Language>();
	@JsonProperty("subjects")
	private List<String> subjects = new ArrayList<String>();
	@JsonProperty("publish_country")
	private String publishCountry;
	@JsonProperty("by_statement")
	private String byStatement;
	@JsonProperty("oclc_numbers")
	private List<String> oclcNumbers = new ArrayList<String>();
	@JsonProperty("type")
	private Type_ type;
	@JsonProperty("physical_dimensions")
	private String physicalDimensions;
	@JsonProperty("revision")
	private Integer revision;
	@JsonProperty("publishers")
	private List<String> publishers = new ArrayList<String>();
	@JsonProperty("description")
	private String description;
	@JsonProperty("physical_format")
	private String physicalFormat;
	@JsonProperty("last_modified")
	private LastModified lastModified;
	@JsonProperty("key")
	private String key;
	@JsonProperty("authors")
	private List<Author> authors = new ArrayList<Author>();
	@JsonProperty("publish_places")
	private List<String> publishPlaces = new ArrayList<String>();
	@JsonProperty("pagination")
	private String pagination;
	@JsonProperty("classifications")
	private Classifications classifications;
	@JsonProperty("created")
	private Created created;
	@JsonProperty("lccn")
	private List<String> lccn = new ArrayList<String>();
	@JsonProperty("notes")
	private Notes notes;
	@JsonProperty("identifiers")
	private Identifiers identifiers;
	@JsonProperty("isbn_13")
	private List<String> isbn13 = new ArrayList<String>();
	@JsonProperty("dewey_decimal_class")
	private List<String> deweyDecimalClass = new ArrayList<String>();
	@JsonProperty("isbn_10")
	private List<String> isbn10 = new ArrayList<String>();
	@JsonProperty("publish_date")
	private String publishDate;
	@JsonProperty("works")
	private List<Work> works = new ArrayList<Work>();
	@JsonIgnore
	private final Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 *
	 * @return The numberOfPages
	 */
	@JsonProperty("number_of_pages")
	public Integer getNumberOfPages() {
		return numberOfPages;
	}

	/**
	 *
	 * @param numberOfPages
	 *            The number_of_pages
	 */
	@JsonProperty("number_of_pages")
	public void setNumberOfPages(final Integer numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	/**
	 *
	 * @return The tableOfContents
	 */
	@JsonProperty("table_of_contents")
	public List<TableOfContent> getTableOfContents() {
		return tableOfContents;
	}

	/**
	 *
	 * @param tableOfContents
	 *            The table_of_contents
	 */
	@JsonProperty("table_of_contents")
	public void setTableOfContents(final List<TableOfContent> tableOfContents) {
		this.tableOfContents = tableOfContents;
	}

	/**
	 *
	 * @return The weight
	 */
	@JsonProperty("weight")
	public String getWeight() {
		return weight;
	}

	/**
	 *
	 * @param weight
	 *            The weight
	 */
	@JsonProperty("weight")
	public void setWeight(final String weight) {
		this.weight = weight;
	}

	/**
	 *
	 * @return The covers
	 */
	@JsonProperty("covers")
	public List<Integer> getCovers() {
		return covers;
	}

	/**
	 *
	 * @param covers
	 *            The covers
	 */
	@JsonProperty("covers")
	public void setCovers(final List<Integer> covers) {
		this.covers = covers;
	}

	/**
	 *
	 * @return The lcClassifications
	 */
	@JsonProperty("lc_classifications")
	public List<String> getLcClassifications() {
		return lcClassifications;
	}

	/**
	 *
	 * @param lcClassifications
	 *            The lc_classifications
	 */
	@JsonProperty("lc_classifications")
	public void setLcClassifications(final List<String> lcClassifications) {
		this.lcClassifications = lcClassifications;
	}

	/**
	 *
	 * @return The latestRevision
	 */
	@JsonProperty("latest_revision")
	public Integer getLatestRevision() {
		return latestRevision;
	}

	/**
	 *
	 * @param latestRevision
	 *            The latest_revision
	 */
	@JsonProperty("latest_revision")
	public void setLatestRevision(final Integer latestRevision) {
		this.latestRevision = latestRevision;
	}

	/**
	 *
	 * @return The ocaid
	 */
	@JsonProperty("ocaid")
	public String getOcaid() {
		return ocaid;
	}

	/**
	 *
	 * @param ocaid
	 *            The ocaid
	 */
	@JsonProperty("ocaid")
	public void setOcaid(final String ocaid) {
		this.ocaid = ocaid;
	}

	/**
	 *
	 * @return The contributors
	 */
	@JsonProperty("contributors")
	public List<Contributor> getContributors() {
		return contributors;
	}

	/**
	 *
	 * @param contributors
	 *            The contributors
	 */
	@JsonProperty("contributors")
	public void setContributors(final List<Contributor> contributors) {
		this.contributors = contributors;
	}

	/**
	 *
	 * @return The sourceRecords
	 */
	@JsonProperty("source_records")
	public List<String> getSourceRecords() {
		return sourceRecords;
	}

	/**
	 *
	 * @param sourceRecords
	 *            The source_records
	 */
	@JsonProperty("source_records")
	public void setSourceRecords(final List<String> sourceRecords) {
		this.sourceRecords = sourceRecords;
	}

	/**
	 *
	 * @return The title
	 */
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	/**
	 *
	 * @param title
	 *            The title
	 */
	@JsonProperty("title")
	public void setTitle(final String title) {
		this.title = title;
	}

	/**
	 *
	 * @return The languages
	 */
	@JsonProperty("languages")
	public List<Language> getLanguages() {
		return languages;
	}

	/**
	 *
	 * @param languages
	 *            The languages
	 */
	@JsonProperty("languages")
	public void setLanguages(final List<Language> languages) {
		this.languages = languages;
	}

	/**
	 *
	 * @return The subjects
	 */
	@JsonProperty("subjects")
	public List<String> getSubjects() {
		return subjects;
	}

	/**
	 *
	 * @param subjects
	 *            The subjects
	 */
	@JsonProperty("subjects")
	public void setSubjects(final List<String> subjects) {
		this.subjects = subjects;
	}

	/**
	 *
	 * @return The publishCountry
	 */
	@JsonProperty("publish_country")
	public String getPublishCountry() {
		return publishCountry;
	}

	/**
	 *
	 * @param publishCountry
	 *            The publish_country
	 */
	@JsonProperty("publish_country")
	public void setPublishCountry(final String publishCountry) {
		this.publishCountry = publishCountry;
	}

	/**
	 *
	 * @return The byStatement
	 */
	@JsonProperty("by_statement")
	public String getByStatement() {
		return byStatement;
	}

	/**
	 *
	 * @param byStatement
	 *            The by_statement
	 */
	@JsonProperty("by_statement")
	public void setByStatement(final String byStatement) {
		this.byStatement = byStatement;
	}

	/**
	 *
	 * @return The oclcNumbers
	 */
	@JsonProperty("oclc_numbers")
	public List<String> getOclcNumbers() {
		return oclcNumbers;
	}

	/**
	 *
	 * @param oclcNumbers
	 *            The oclc_numbers
	 */
	@JsonProperty("oclc_numbers")
	public void setOclcNumbers(final List<String> oclcNumbers) {
		this.oclcNumbers = oclcNumbers;
	}

	/**
	 *
	 * @return The type
	 */
	@JsonProperty("type")
	public Type_ getType() {
		return type;
	}

	/**
	 *
	 * @param type
	 *            The type
	 */
	@JsonProperty("type")
	public void setType(final Type_ type) {
		this.type = type;
	}

	/**
	 *
	 * @return The physicalDimensions
	 */
	@JsonProperty("physical_dimensions")
	public String getPhysicalDimensions() {
		return physicalDimensions;
	}

	/**
	 *
	 * @param physicalDimensions
	 *            The physical_dimensions
	 */
	@JsonProperty("physical_dimensions")
	public void setPhysicalDimensions(final String physicalDimensions) {
		this.physicalDimensions = physicalDimensions;
	}

	/**
	 *
	 * @return The revision
	 */
	@JsonProperty("revision")
	public Integer getRevision() {
		return revision;
	}

	/**
	 *
	 * @param revision
	 *            The revision
	 */
	@JsonProperty("revision")
	public void setRevision(final Integer revision) {
		this.revision = revision;
	}

	/**
	 *
	 * @return The publishers
	 */
	@JsonProperty("publishers")
	public List<String> getPublishers() {
		return publishers;
	}

	/**
	 *
	 * @param publishers
	 *            The publishers
	 */
	@JsonProperty("publishers")
	public void setPublishers(final List<String> publishers) {
		this.publishers = publishers;
	}

	/**
	 *
	 * @return The description
	 */
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	/**
	 *
	 * @param description
	 *            The description
	 */
	@JsonProperty("description")
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 *
	 * @return The physicalFormat
	 */
	@JsonProperty("physical_format")
	public String getPhysicalFormat() {
		return physicalFormat;
	}

	/**
	 *
	 * @param physicalFormat
	 *            The physical_format
	 */
	@JsonProperty("physical_format")
	public void setPhysicalFormat(final String physicalFormat) {
		this.physicalFormat = physicalFormat;
	}

	/**
	 *
	 * @return The lastModified
	 */
	@JsonProperty("last_modified")
	public LastModified getLastModified() {
		return lastModified;
	}

	/**
	 *
	 * @param lastModified
	 *            The last_modified
	 */
	@JsonProperty("last_modified")
	public void setLastModified(final LastModified lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 *
	 * @return The key
	 */
	@JsonProperty("key")
	public String getKey() {
		return key;
	}

	/**
	 *
	 * @param key
	 *            The key
	 */
	@JsonProperty("key")
	public void setKey(final String key) {
		this.key = key;
	}

	/**
	 *
	 * @return The authors
	 */
	@JsonProperty("authors")
	public List<Author> getAuthors() {
		return authors;
	}

	/**
	 *
	 * @param authors
	 *            The authors
	 */
	@JsonProperty("authors")
	public void setAuthors(final List<Author> authors) {
		this.authors = authors;
	}

	/**
	 *
	 * @return The publishPlaces
	 */
	@JsonProperty("publish_places")
	public List<String> getPublishPlaces() {
		return publishPlaces;
	}

	/**
	 *
	 * @param publishPlaces
	 *            The publish_places
	 */
	@JsonProperty("publish_places")
	public void setPublishPlaces(final List<String> publishPlaces) {
		this.publishPlaces = publishPlaces;
	}

	/**
	 *
	 * @return The pagination
	 */
	@JsonProperty("pagination")
	public String getPagination() {
		return pagination;
	}

	/**
	 *
	 * @param pagination
	 *            The pagination
	 */
	@JsonProperty("pagination")
	public void setPagination(final String pagination) {
		this.pagination = pagination;
	}

	/**
	 *
	 * @return The classifications
	 */
	@JsonProperty("classifications")
	public Classifications getClassifications() {
		return classifications;
	}

	/**
	 *
	 * @param classifications
	 *            The classifications
	 */
	@JsonProperty("classifications")
	public void setClassifications(final Classifications classifications) {
		this.classifications = classifications;
	}

	/**
	 *
	 * @return The created
	 */
	@JsonProperty("created")
	public Created getCreated() {
		return created;
	}

	/**
	 *
	 * @param created
	 *            The created
	 */
	@JsonProperty("created")
	public void setCreated(final Created created) {
		this.created = created;
	}

	/**
	 *
	 * @return The lccn
	 */
	@JsonProperty("lccn")
	public List<String> getLccn() {
		return lccn;
	}

	/**
	 *
	 * @param lccn
	 *            The lccn
	 */
	@JsonProperty("lccn")
	public void setLccn(final List<String> lccn) {
		this.lccn = lccn;
	}

	/**
	 *
	 * @return The notes
	 */
	@JsonProperty("notes")
	public Notes getNotes() {
		return notes;
	}

	/**
	 *
	 * @param notes
	 *            The notes
	 */
	@JsonProperty("notes")
	public void setNotes(final Notes notes) {
		this.notes = notes;
	}

	/**
	 *
	 * @return The identifiers
	 */
	@JsonProperty("identifiers")
	public Identifiers getIdentifiers() {
		return identifiers;
	}

	/**
	 *
	 * @param identifiers
	 *            The identifiers
	 */
	@JsonProperty("identifiers")
	public void setIdentifiers(final Identifiers identifiers) {
		this.identifiers = identifiers;
	}

	/**
	 *
	 * @return The isbn13
	 */
	@JsonProperty("isbn_13")
	public List<String> getIsbn13() {
		return isbn13;
	}

	/**
	 *
	 * @param isbn13
	 *            The isbn_13
	 */
	@JsonProperty("isbn_13")
	public void setIsbn13(final List<String> isbn13) {
		this.isbn13 = isbn13;
	}

	/**
	 *
	 * @return The deweyDecimalClass
	 */
	@JsonProperty("dewey_decimal_class")
	public List<String> getDeweyDecimalClass() {
		return deweyDecimalClass;
	}

	/**
	 *
	 * @param deweyDecimalClass
	 *            The dewey_decimal_class
	 */
	@JsonProperty("dewey_decimal_class")
	public void setDeweyDecimalClass(final List<String> deweyDecimalClass) {
		this.deweyDecimalClass = deweyDecimalClass;
	}

	/**
	 *
	 * @return The isbn10
	 */
	@JsonProperty("isbn_10")
	public List<String> getIsbn10() {
		return isbn10;
	}

	/**
	 *
	 * @param isbn10
	 *            The isbn_10
	 */
	@JsonProperty("isbn_10")
	public void setIsbn10(final List<String> isbn10) {
		this.isbn10 = isbn10;
	}

	/**
	 *
	 * @return The publishDate
	 */
	@JsonProperty("publish_date")
	public String getPublishDate() {
		return publishDate;
	}

	/**
	 *
	 * @param publishDate
	 *            The publish_date
	 */
	@JsonProperty("publish_date")
	public void setPublishDate(final String publishDate) {
		this.publishDate = publishDate;
	}

	/**
	 *
	 * @return The works
	 */
	@JsonProperty("works")
	public List<Work> getWorks() {
		return works;
	}

	/**
	 *
	 * @param works
	 *            The works
	 */
	@JsonProperty("works")
	public void setWorks(final List<Work> works) {
		this.works = works;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(final String name, final Object value) {
		this.additionalProperties.put(name, value);
	}

}
