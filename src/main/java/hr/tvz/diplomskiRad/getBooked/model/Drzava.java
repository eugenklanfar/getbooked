package hr.tvz.diplomskiRad.getBooked.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Model za drzavu.
 * @author eugen
 *
 */

@Entity
@Table(name = "Drzava")
public class Drzava {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank(message = "")
	@Column(name = "iso", nullable = false)
	private String iso;
	
	@NotBlank(message = "")
	@Column(name = "name", nullable = false)
	private String name;
	
	@NotBlank(message = "")
	@Column(name = "niceName", nullable = false)
	private String niceName;
	
	@Column(name = "isoThree", nullable = true)
	private String isoThree;
	
	@Column(name = "numcode", nullable = true)
	private Integer numcode;
	
	@NotBlank(message = "")
	@Column(name = "phonecode", nullable = false)
	private Integer phonecode;
	
	@OneToMany(mappedBy = "drzava")
	private Set<Korisnik> korisnici;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIso() {
		return iso;
	}

	public void setIso(String iso) {
		this.iso = iso;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNiceName() {
		return niceName;
	}

	public void setNiceName(String niceName) {
		this.niceName = niceName;
	}

	public String getIsoThree() {
		return isoThree;
	}

	public void setIsoThree(String isoThree) {
		this.isoThree = isoThree;
	}

	public Integer getNumcode() {
		return numcode;
	}

	public void setNumcode(Integer numcode) {
		this.numcode = numcode;
	}

	public Integer getPhonecode() {
		return phonecode;
	}

	public void setPhonecode(Integer phonecode) {
		this.phonecode = phonecode;
	}

	public Set<Korisnik> getKorisnici() {
		return korisnici;
	}

	public void setKorisnici(Set<Korisnik> korisnici) {
		this.korisnici = korisnici;
	}

}
