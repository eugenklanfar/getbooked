package hr.tvz.diplomskiRad.getBooked.model;

/**
 * Enumeracija za pripadajuce aktivnosti korisnika.
 * @author eugen
 *
 */
public enum PovijestCitanjaVrsta {
	PLANIRAM("PLANIRAM"), CITAM("CITAM"), PROCITANO("PROCITANO");
	
	String povijestCitanjaTip;
	
	private PovijestCitanjaVrsta(final String povijestCitanjaTip){
		this.povijestCitanjaTip = povijestCitanjaTip;
	}
	
	public String getPovijestCitanjaTip() {
		return povijestCitanjaTip;
	}

}
