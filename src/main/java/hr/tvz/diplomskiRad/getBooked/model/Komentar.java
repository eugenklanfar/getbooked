package hr.tvz.diplomskiRad.getBooked.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Model za komentar korisnika na odabranu knjigu.
 * @author eugen
 *
 */
@Entity
@Table(name = "Komentar")
public class Komentar {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Size(min = 0, max = 50, message="Naslov komentara mora sadržavati između 0 do 50 znakova.")
	@Column(name = "naslov", unique = false, nullable = true)
	private String naslov;
	
	@NotBlank(message="")
	@Size(min = 1, max = 1000, message="Tekst komentara mora sadržavati između 1 do 1000 znakova.")
	@Column(name = "tekst", unique = false, nullable = false)
	private String tekst;
	
	@NotBlank(message="")
	@Column(name = "datumObjave", unique = false, nullable = false)
	private String datumObjave;
	
	@Column(name = "upVote", nullable = true)
	private int upVote = 0;
	
	@Column(name = "downVote", nullable = true)
	private int downVote = 0;
	
	@NotNull
	@Column(name="obrisan", nullable = false)
	private Boolean obrisan = false;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "korisnik_id")
	private Korisnik korisnik;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "knjiga_id")
	private Knjiga knjiga;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "vijest_id")
	private Vijest vijest;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaslov() {
		return naslov;
	}

	public void setNaslov(String naslov) {
		this.naslov = naslov;
	}

	public String getTekst() {
		return tekst;
	}

	public void setTekst(String tekst) {
		this.tekst = tekst;
	}

	public String getDatumObjave() {
		return datumObjave;
	}

	public void setDatumObjave(String datumObjave) {
		this.datumObjave = datumObjave;
	}

	public int getUpVote() {
		return upVote;
	}

	public void setUpVote(int upVote) {
		this.upVote = upVote;
	}

	public int getDownVote() {
		return downVote;
	}

	public void setDownVote(int downVote) {
		this.downVote = downVote;
	}

	public Boolean getObrisan() {
		return obrisan;
	}

	public void setObrisan(Boolean obrisan) {
		this.obrisan = obrisan;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Knjiga getKnjiga() {
		return knjiga;
	}

	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}

	public Vijest getVijest() {
		return vijest;
	}

	public void setVijest(Vijest vijest) {
		this.vijest = vijest;
	}

}
