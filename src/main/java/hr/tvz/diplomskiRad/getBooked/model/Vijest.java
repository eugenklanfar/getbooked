package hr.tvz.diplomskiRad.getBooked.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Model za vijest
 * 
 * @author eugen
 *
 */
@Entity
@Table(name = "Vijest")
public class Vijest {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Size(min = 1, max = 300, message = "Naslov vijesti mora sadržavati između 1 do 100 znakova.")
	@Column(name = "naslov", unique = false, nullable = false)
	private String naslov;

	@Size(min = 1, max = 400, message = "Podnaslov vijesti mora sadržavati između 1 do 100 znakova.")
	@Column(name = "podNaslov", unique = false, nullable = false)
	private String podNaslov;

	@NotBlank(message = "")
	@Size(min = 1, max = 10000, message = "Tekst vijesti mora sadržavati između 1 do 10000 znakova.")
	@Column(name = "tekst", unique = false, nullable = false)
	private String tekst;

	@NotBlank(message = "")
	@Column(name = "datumObjave", unique = false, nullable = false)
	private String datumObjave;

	@NotEmpty
	@Column(name = "status", nullable = false)
	private String status = Status.AKTIVAN.getStatus();

	@NotBlank(message = "")
	@Size(min = 2, max = 80, message = "Ime i prezime mora sadržavati između 2 do 80 znakova.")
	@Column(name = "imePrezime", nullable = false)
	private String imePrezime;

	@OneToMany(mappedBy = "vijest")
	private Set<Komentar> komentari;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaslov() {
		return naslov;
	}

	public void setNaslov(String naslov) {
		this.naslov = naslov;
	}

	public String getPodNaslov() {
		return podNaslov;
	}

	public void setPodNaslov(String podNaslov) {
		this.podNaslov = podNaslov;
	}

	public String getTekst() {
		return tekst;
	}

	public void setTekst(String tekst) {
		this.tekst = tekst;
	}

	public String getDatumObjave() {
		return datumObjave;
	}

	public void setDatumObjave(String datumObjave) {
		this.datumObjave = datumObjave;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getImePrezime() {
		return imePrezime;
	}

	public void setImePrezime(String imePrezime) {
		this.imePrezime = imePrezime;
	}

	public Set<Komentar> getKomentari() {
		return komentari;
	}

	public void setKomentari(Set<Komentar> komentari) {
		this.komentari = komentari;
	}

}
