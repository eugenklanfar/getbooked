package hr.tvz.diplomskiRad.getBooked.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Model za autora knjige.
 * 
 * @author eugen
 *
 */
@Entity
@Table(name = "Autor")
public class Autor {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotBlank(message = "")
	@Size(min = 2, max = 50, message = "Ime i prezime mora sadržavati između 2 do 100 znakova.")
	@Column(name = "imePrezime", unique = false, nullable = false)
	private String imePrezime;

	@Size(min = 0, max = 50)
	@Column(name = "datum_rodenja", unique = false, nullable = true)
	private String datumRodenja;

	@Size(min = 0, max = 50)
	@Column(name = "datum_smrti", unique = false, nullable = true)
	private String datumSmrti;

	@Size(max = 100)
	@Column(name = "open_library_key", nullable = true)
	private String openLibraryKey;

	@Size(max = 1000)
	@Column(name = "napomena", unique = false, nullable = true)
	private String napomena;

	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "autori")
	private Set<Knjiga> knjige;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImePrezime() {
		return imePrezime;
	}

	public void setImePrezime(String imePrezime) {
		this.imePrezime = imePrezime;
	}

	/*
	 * public String getPrezime() { return prezime; }
	 * 
	 * public void setPrezime(String prezime) { this.prezime = prezime; }
	 */

	public String getDatumRodenja() {
		return datumRodenja;
	}

	public void setDatumRodenja(String datumRodenja) {
		this.datumRodenja = datumRodenja;
	}

	public String getDatumSmrti() {
		return datumSmrti;
	}

	public void setDatumSmrti(String datumSmrti) {
		this.datumSmrti = datumSmrti;
	}

	public String getOpenLibraryKey() {
		return openLibraryKey;
	}

	public void setOpenLibraryKey(String openLibraryKey) {
		this.openLibraryKey = openLibraryKey;
	}

	public String getNapomena() {
		return napomena;
	}

	public void setNapomena(String napomena) {
		this.napomena = napomena;
	}

	public Set<Knjiga> getKnjige() {
		return knjige;
	}

	public void setKnjige(Set<Knjiga> knjige) {
		this.knjige = knjige;
	}

}
