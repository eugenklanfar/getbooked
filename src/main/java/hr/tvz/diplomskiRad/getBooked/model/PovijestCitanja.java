package hr.tvz.diplomskiRad.getBooked.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Model za aktivnost korisnika koja može biti jedna od odabranih iz enumeracije @AktivnostVrsta
 * @author eugen
 *
 */
@Entity
@Table(name = "PovijestCitanja")
public class PovijestCitanja {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "knjiga_id")
	private Knjiga knjiga;
	
	@NotEmpty
	@Column(name = "vrsta", nullable = false)
	private String vrsta = PovijestCitanjaVrsta.PLANIRAM.getPovijestCitanjaTip();
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "korisnik_id")
	private Korisnik korisnik;
	
	@NotNull
	@Column(name = "aktivan", nullable = false)
	private Boolean aktivan = true;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Knjiga getKnjiga() {
		return knjiga;
	}

	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}

	public String getVrsta() {
		return vrsta;
	}

	public void setVrsta(String vrsta) {
		this.vrsta = vrsta;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Boolean getAktivan() {
		return aktivan;
	}

	public void setAktivan(Boolean aktivan) {
		this.aktivan = aktivan;
	}
	
}
