package hr.tvz.diplomskiRad.getBooked.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

/**
 * Model za ocjenu koju korisnik daje odabranoj knjizi.
 * 
 * @author eugen
 *
 */
@Entity
@Table(name = "Ocjena")
public class Ocjena {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Range(min = 1, max = 5)
	@Column(name = "ocjena", nullable = false)
	private int ocjena;

	@NotBlank(message = "")
	@Column(name = "datum_ocjene", unique = false, nullable = false)
	private String datumOcjene;

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "korisnik_id")
	private Korisnik korisnik;

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "knjiga_id")
	private Knjiga knjiga;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getOcjena() {
		return ocjena;
	}

	public void setOcjena(int ocjena) {
		this.ocjena = ocjena;
	}

	public String getDatumOcjene() {
		return datumOcjene;
	}

	public void setDatumOcjene(String datumOcjene) {
		this.datumOcjene = datumOcjene;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Knjiga getKnjiga() {
		return knjiga;
	}

	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}

}
