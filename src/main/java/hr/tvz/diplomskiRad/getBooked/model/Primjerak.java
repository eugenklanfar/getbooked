package hr.tvz.diplomskiRad.getBooked.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "Primjerak")
public class Primjerak {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@NotEmpty
	@Column(name = "status", nullable = false)
	private String status = Status.AKTIVAN.getStatus();

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "knjiga_id")
	private Knjiga knjiga;

	@OneToMany(mappedBy = "primjerak")
	private Set<Aktivnost> aktivnost;

	@Column(name = "napomena", nullable = true)
	@Size(max = 1000)
	private String napomena;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Knjiga getKnjiga() {
		return knjiga;
	}

	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}

	public Set<Aktivnost> getAktivnost() {
		return aktivnost;
	}

	public void setAktivnost(Set<Aktivnost> aktivnost) {
		this.aktivnost = aktivnost;
	}

	public String getNapomena() {
		return napomena;
	}

	public void setNapomena(String napomena) {
		this.napomena = napomena;
	}

	public boolean isPosudbaMoguca() {
		if (status.equals(Status.AKTIVAN.getStatus())) {
			return true;
		}
		if (status.equals(Status.REZERVIRAN.getStatus())) {
			return true;
		}
		return false;
	}

	public boolean isRezervacijaMoguca() {
		if (status.equals(Status.AKTIVAN.getStatus())) {
			return true;
		}
		return false;
	}

	public boolean isRezerviran() {
		return status.equals(Status.REZERVIRAN.getStatus());
	}

	public boolean isAktivan() {
		return status.equals(Status.AKTIVAN.getStatus());
	}

}
