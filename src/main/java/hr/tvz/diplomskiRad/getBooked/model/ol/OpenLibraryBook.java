package hr.tvz.diplomskiRad.getBooked.model.ol;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import hr.tvz.diplomskiRad.getBooked.model.ol.Details;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "info_url", "bib_key", "preview_url", "thumbnail_url", "details", "preview" })
public class OpenLibraryBook {

	@JsonProperty("info_url")
	private String infoUrl;
	@JsonProperty("bib_key")
	private String bibKey;
	@JsonProperty("preview_url")
	private String previewUrl;
	@JsonProperty("thumbnail_url")
	private String thumbnailUrl;
	@JsonProperty("details")
	private Details details;
	@JsonProperty("preview")
	private String preview;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 *
	 * @return The infoUrl
	 */
	@JsonProperty("info_url")
	public String getInfoUrl() {
		return infoUrl;
	}

	/**
	 *
	 * @param infoUrl
	 *            The info_url
	 */
	@JsonProperty("info_url")
	public void setInfoUrl(String infoUrl) {
		this.infoUrl = infoUrl;
	}

	/**
	 *
	 * @return The bibKey
	 */
	@JsonProperty("bib_key")
	public String getBibKey() {
		return bibKey;
	}

	/**
	 *
	 * @param bibKey
	 *            The bib_key
	 */
	@JsonProperty("bib_key")
	public void setBibKey(String bibKey) {
		this.bibKey = bibKey;
	}

	/**
	 *
	 * @return The previewUrl
	 */
	@JsonProperty("preview_url")
	public String getPreviewUrl() {
		return previewUrl;
	}

	/**
	 *
	 * @param previewUrl
	 *            The preview_url
	 */
	@JsonProperty("preview_url")
	public void setPreviewUrl(String previewUrl) {
		this.previewUrl = previewUrl;
	}

	/**
	 *
	 * @return The thumbnailUrl
	 */
	@JsonProperty("thumbnail_url")
	public String getThumbnailUrl() {
		return thumbnailUrl;
	}

	/**
	 *
	 * @param thumbnailUrl
	 *            The thumbnail_url
	 */
	@JsonProperty("thumbnail_url")
	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}

	/**
	 *
	 * @return The details
	 */
	@JsonProperty("details")
	public Details getDetails() {
		return details;
	}

	/**
	 *
	 * @param details
	 *            The details
	 */
	@JsonProperty("details")
	public void setDetails(Details details) {
		this.details = details;
	}

	/**
	 *
	 * @return The preview
	 */
	@JsonProperty("preview")
	public String getPreview() {
		return preview;
	}

	/**
	 *
	 * @param preview
	 *            The preview
	 */
	@JsonProperty("preview")
	public void setPreview(String preview) {
		this.preview = preview;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
	
}
