package hr.tvz.diplomskiRad.getBooked.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hr.tvz.diplomskiRad.getBooked.model.Aktivnost;

@Repository
public interface AktivnostRepository extends CrudRepository<Aktivnost, Long> {
	
	Aktivnost findById(Long id);
	
	List<Aktivnost> findByKorisnickoImeAndVrsta(String username, String aktivnostTip);
	
	List<Aktivnost> findByKorisnickoImeAndVrstaAndDatumDoIsNull(String username, String aktivnostTip);

	List<Aktivnost> findByVrstaAndDatumDoIsNull(String vrsta);

}
