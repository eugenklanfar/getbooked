package hr.tvz.diplomskiRad.getBooked.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hr.tvz.diplomskiRad.getBooked.model.PovijestCitanja;
import hr.tvz.diplomskiRad.getBooked.model.Knjiga;
import hr.tvz.diplomskiRad.getBooked.model.Korisnik;

/**
 * Veza na bazu za @PovijestCitanja.
 * @author eugen
 *
 */
@Repository
public interface PovijestCitanjaRepository extends CrudRepository<PovijestCitanja, Long> {
	
	List<PovijestCitanja> findAll();
	
	PovijestCitanja findById(Long id);
	
	PovijestCitanja findByKorisnikAndKnjiga(Korisnik korisnik, Knjiga knjiga);
	
	List<PovijestCitanja> findByKorisnikAndVrsta(Korisnik korisnik, String vrsta);

}
