package hr.tvz.diplomskiRad.getBooked.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hr.tvz.diplomskiRad.getBooked.model.Isbn;

@Repository
public interface IsbnRepository extends CrudRepository<Isbn, Long> {
	
	List<Isbn> findAll();

}
