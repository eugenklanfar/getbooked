package hr.tvz.diplomskiRad.getBooked.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hr.tvz.diplomskiRad.getBooked.model.Zanr;

@Repository
public interface ZanrRepository extends CrudRepository<Zanr, Long> {

	List<Zanr> findAll();
}
