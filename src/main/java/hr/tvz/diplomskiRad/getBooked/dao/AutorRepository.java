package hr.tvz.diplomskiRad.getBooked.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hr.tvz.diplomskiRad.getBooked.model.Autor;

/**
 * Veza na bazu za @Autor.
 * @author eugen
 *
 */
@Repository
public interface AutorRepository extends CrudRepository<Autor, Long> {
	
	Autor findById(Long id);
	
	List<Autor> findByKnjigeId(Long knjigaId);
	
	List<Autor> findByImePrezime(String imePrezime);
	
	List<Autor> findAll();

}
