package hr.tvz.diplomskiRad.getBooked.dao;

import org.springframework.data.repository.CrudRepository;

import hr.tvz.diplomskiRad.getBooked.model.Primjerak;

public interface PrimjerakRepository extends CrudRepository<Primjerak, Long> {
	
	Primjerak findById(Long id);

}
