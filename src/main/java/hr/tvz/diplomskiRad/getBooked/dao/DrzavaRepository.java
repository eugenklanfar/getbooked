package hr.tvz.diplomskiRad.getBooked.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hr.tvz.diplomskiRad.getBooked.model.Drzava;

@Repository
public interface DrzavaRepository extends CrudRepository<Drzava, Long> {
	
	List<Drzava> findAll();
}
