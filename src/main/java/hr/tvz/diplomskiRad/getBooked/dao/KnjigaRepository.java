package hr.tvz.diplomskiRad.getBooked.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hr.tvz.diplomskiRad.getBooked.model.Knjiga;

/**
 * Veza na bazu za @Knjiga.
 * @author eugen
 *
 */
@Repository
public interface KnjigaRepository extends CrudRepository<Knjiga, Long> {
	
	List<Knjiga> findAll();
	
	List<Knjiga> findByOrderByNazivAsc();

	Knjiga findById(Long id);
	
	Knjiga findByISBN(String ISBN);
	
	void saveAndFlush(Knjiga knjiga);
}
