package hr.tvz.diplomskiRad.getBooked.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hr.tvz.diplomskiRad.getBooked.model.Vijest;

@Repository
public interface VijestRepository extends CrudRepository<Vijest, Long> {
	
	List<Vijest> findByOrderByDatumObjaveDesc();

}
