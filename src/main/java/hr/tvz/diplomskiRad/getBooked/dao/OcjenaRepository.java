package hr.tvz.diplomskiRad.getBooked.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hr.tvz.diplomskiRad.getBooked.model.Knjiga;
import hr.tvz.diplomskiRad.getBooked.model.Korisnik;
import hr.tvz.diplomskiRad.getBooked.model.Ocjena;

@Repository
public interface OcjenaRepository extends CrudRepository<Ocjena, Long> {
	Ocjena findById(Long id);
	
	Ocjena findByKorisnikAndKnjiga(Korisnik korisnik, Knjiga knjiga);
	
	List<Ocjena> findByKorisnik(Korisnik korisnik);
	
	List<Ocjena> findByKorisnikIdOrderByDatumOcjene(Long id);
}
