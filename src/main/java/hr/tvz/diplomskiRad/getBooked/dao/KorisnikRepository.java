package hr.tvz.diplomskiRad.getBooked.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hr.tvz.diplomskiRad.getBooked.model.Korisnik;

/**
 * Veza na bazu za @Korisnik.
 * @author eugen
 *
 */
@Repository
public interface KorisnikRepository extends CrudRepository<Korisnik, Long> {
	
	Korisnik findById(Long id);

	Korisnik findByKorisnickoIme(String korisnickoIme);
	
	List<Korisnik> findAll();
}
