package hr.tvz.diplomskiRad.getBooked.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hr.tvz.diplomskiRad.getBooked.model.Komentar;

@Repository
public interface KomentarRepository extends CrudRepository<Komentar, Long> {
	
	List <Komentar> findByKorisnikIdOrderByDatumObjaveAsc(Long id);
	
	List <Komentar> findByKorisnikIdAndVijestIdIsNullOrderByDatumObjaveAsc(Long id);
	
	List <Komentar> findByKorisnikIdAndKnjigaIdIsNullOrderByDatumObjaveAsc(Long id);

}
