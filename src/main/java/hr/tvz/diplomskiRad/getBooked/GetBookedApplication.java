package hr.tvz.diplomskiRad.getBooked;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GetBookedApplication {

	public static void main(String[] args) {
		SpringApplication.run(GetBookedApplication.class, args);
	}
}
