package hr.tvz.diplomskiRad.getBooked.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * Konfiguracija za postavljanje sigurnosnih postavki
 * @author eugen
 *
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	@Qualifier("customUserDetailsService")
	private UserDetailsService userDetailsService;
	
	/**
	 * Postavljanje pristupa stranicama u ovisnosti o korisnickoj roli
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception{
		http.authorizeRequests()
		.antMatchers("/register", "/img/*", "/css/*", "/js/*").permitAll()
		.antMatchers("/", "/index", "/profile", "/listaKnjiga", "/knjigaDetalji", "/listaAutora", "/autorDetalji", "/profileDetalji", "/vijestDetalji").hasAnyRole("KORISNIK", "ADMIN")
		.antMatchers("/addAutor", "/addKnjiga", "/editKnjiga", "/addKnjigaAutor", "/removeKnjigaAutor", "/editAutor", "/openLibraryForm", "/addOpenLibraryKnjiga", "/addPrimjerak", "/addPrimjerakToKnjiga", "/listaAktivnosti", "/posudbaForm", "/vijestForm").hasRole("ADMIN")
		.anyRequest().authenticated()
        .and()
    .formLogin()
        .loginPage("/login")
        .permitAll()
        .and()
    .logout()
        .permitAll();
		
		/**
		 * Preusmjerevanje u slučaju uspješne, odnosno neuspješne prijave u sustav.
		 */
		http.formLogin().loginPage("/login").successHandler(successHandler()).defaultSuccessUrl("/index")
        .failureUrl("/login?error=1").permitAll();

		/**
		 * Preusmjeravanje pri odjavi sa sustava,
		 */
		http.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout", "GET")).logoutSuccessUrl("/login")
        .permitAll();
		http.exceptionHandling().accessDeniedPage("/Access_Denied");
		
	}
	
	@Autowired
	public void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
		auth.authenticationProvider(authenticationProvider());
	}

	@Bean
	public AuthenticationProvider authenticationProvider() {
		final DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setUserDetailsService(userDetailsService);
		authenticationProvider.setPasswordEncoder(passwordEncoder());
		return authenticationProvider;
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public AuthenticationSuccessHandler successHandler() {
		return new SavedRequestAwareAuthenticationSuccessHandler();
	}

}
